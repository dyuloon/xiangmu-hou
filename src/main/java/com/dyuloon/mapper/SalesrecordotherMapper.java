package com.dyuloon.mapper;

import com.dyuloon.entity.Salesrecordother;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
public interface SalesrecordotherMapper extends BaseMapper<Salesrecordother> {

}
