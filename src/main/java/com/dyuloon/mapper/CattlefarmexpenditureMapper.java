package com.dyuloon.mapper;

import com.dyuloon.entity.Cattlefarmexpenditure;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
public interface CattlefarmexpenditureMapper extends BaseMapper<Cattlefarmexpenditure> {

}
