package com.dyuloon.mapper;

import com.dyuloon.entity.Employeemanagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
public interface EmployeemanagementMapper extends BaseMapper<Employeemanagement> {

}
