package com.dyuloon.mapper;

import com.dyuloon.entity.Datastatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
public interface DatastatisticsMapper extends BaseMapper<Datastatistics> {

}
