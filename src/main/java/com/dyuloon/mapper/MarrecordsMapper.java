package com.dyuloon.mapper;

import com.dyuloon.entity.Marrecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
public interface MarrecordsMapper extends BaseMapper<Marrecords> {

}
