package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Basicsettings implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String jingyemingcheng;

    private String gongniubianhao;

    private String pinyinjianma;

    private String gouxiaoshangmingcheng;

    private String gouxiangshangbianhao;

    private String gouxiangshangwuzi;

    private String lianxiren;

    private String dianhua;


}
