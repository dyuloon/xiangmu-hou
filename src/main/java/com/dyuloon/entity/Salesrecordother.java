package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Salesrecordother implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String niuchangmingcheng;

    private String chepai;

    private String jinchangshunxu;

    private String riqi;

    private String naiguanhao;

    private String caozuoriqi;


}
