package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Feedinganalysis implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String xiangmu;

    private String chuqishu;

    private String diaobodiaoru;

    private String zhuanqunzhuanru;

    private String gouru;

    private String zhihuan;

    private String neibugouru;

    private String chusheng;

    private String diaobodiaoru2;

    private String zhuanqunzhaunchu;

    private String xiaoji;

    private String neibuxiaoshou;

    private String xiaoshou;

    private String zhihuan2;

    private String feiliuyang;

    private String dangqisiwang;


}
