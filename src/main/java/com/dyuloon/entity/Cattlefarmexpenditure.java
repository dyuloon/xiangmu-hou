package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Cattlefarmexpenditure implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String duniutourichengben;

    private String duniuzongjia;

    private String yuchengniutourichengben;

    private String yuchengniuzongjia;

    private String qingnianniutourichengben;

    private String qingnianniuzongjia;

    private String miruniutourichengbne;

    private String miruniuzongjia;

    private String gannainiutourichengben;

    private String gannainiuzongjia;


}
