package com.dyuloon.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Examinationrecords implements Serializable {

    private static final long serialVersionUID=1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;

    private String guanlihao;

    private String niushe;

    private String chandu;

    private String taici;

    private String zaozhengchan;

    private String sihuotai;

    private String taiyi;

    private String chanhou;

    private String jiancha;

    private String zigong;

    private String luanchao;

    private String jiancharen;

    private String caozuoren;


}
