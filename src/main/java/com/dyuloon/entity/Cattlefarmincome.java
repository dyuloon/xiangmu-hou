package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Cattlefarmincome implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String riqi;

    private String azhongliang;

    private String ajiage;

    private String bzhongliang;

    private String bjiage;

    private String czhongliang;

    private String cjiage;

    private String dzhongliang;

    private String djiage;

    private String ezhongliang;

    private String ejiage;

    private String fzhongliang;

    private String fjiage;


}
