package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Storemanagement implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId("id")
      private Integer id;

    private String leixing;

    private String niuzhishu;

    private String zhuangtai;


}
