package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Delegate implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String guanlihao;

    private String niuhao;

    private String niuchang;

    private String taici;

    private String peizhong;

    private String faqingriqi;

    private String faqingleib;

    private String faxianren;

    private String shifoupeizhong;

    private String weipeiyuanyin;

    private String gongniuhao;

    private String dongjinghao;

    private String shifouxingkong;

    private String peizhhongyuan;

    private String peizhongshi;

    private String shangcifanzhi;


}
