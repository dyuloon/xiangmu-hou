package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Farmprofitanalysis implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String riqi;

    private String lirun;

    private String siliaoduniu;

    private String siliaoyucheng;

    private String siiaoqingnian;

    private String siliaomiru;

    private String siliaogannai;

    private String yaopinduniu;

    private String yaopinyucheng;

    private String yaopinqingnian;

    private String yaopinchengmu;

    private String jingyeduniu;

    private String jingyeyucheng;

    private String jingyeqingnian;

    private String jingyechengmu;

    private String xiaoshouniunai;

    private String xiaoshougongdu;

    private String xiaoshoumudu;

    private String xiaoshouyucheng;

    private String xiaoshouqingnian;

    private String xiaoshouchengmu;


}
