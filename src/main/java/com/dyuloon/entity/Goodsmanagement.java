package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Goodsmanagement implements Serializable {

    private static final long serialVersionUID=1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;

    private String siliangbianhao;

    private String siliangmingcheng;

    private String siliaoleibie;

    private String binyinjianma;

    private String siliaofenlei;


}
