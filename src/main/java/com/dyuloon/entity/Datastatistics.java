package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Datastatistics implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String xiangmu;

    private String yiyue;

    private String eryue;

    private String sanyue;

    private String siyue;

    private String wuyue;

    private String liuyue;

    private String qiyue;

    private String bayue;

    private String jiuyue;

    private String shiyue;

    private String shiyiyue;

    private String shieryue;

    private String heji;

    private String yijidu;

    private String erjidu;

    private String sanjidu;

    private String sijidu;


}
