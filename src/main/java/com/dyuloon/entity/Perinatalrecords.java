package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Perinatalrecords implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

    private String guanlihao;

    private String niushe;

    private String denglushi;

    private String taici;

    private String zhuansheriqi;

    private String zhuanchushe;

    private String zhuanchusheleixing;

    private String zhuanrushe;

    private String zhuanrusheleixing;

    private String zhixingren;

    private String caizuoshijian;


}
