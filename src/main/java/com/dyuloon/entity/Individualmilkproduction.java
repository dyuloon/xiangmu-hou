package com.dyuloon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Individualmilkproduction implements Serializable {

    private static final long serialVersionUID=1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;

    private String guanlihao;

    private String niushebianhao;

    private String niusheleibie;

    private String taici;

    private String miruzhuangtai;

    private String channairiqi;

    private String yicenailiang;

    private String ercenailiang;

    private String dangtiannailiang;

    private String chanduriqi;

    private String nirutianshu;

    private String zaitaitianshu;


}
