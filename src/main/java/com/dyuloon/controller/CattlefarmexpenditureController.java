package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Cattlefarmexpenditure;
import com.dyuloon.entity.Decisionsupport;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.CattlefarmexpenditureService;
import com.dyuloon.service.DecisionsupportService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@RestController
@RequestMapping("/cattlefarmexpenditure")
public class CattlefarmexpenditureController {
    @Autowired
    private CattlefarmexpenditureService abnormaldateService;

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Cattlefarmexpenditure> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.abnormaldateService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Cattlefarmexpenditure delegate){
        boolean update = this.abnormaldateService.updateById(delegate);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

}

