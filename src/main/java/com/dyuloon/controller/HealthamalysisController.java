package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Estrusrecord;
import com.dyuloon.entity.Healthamalysis;
import com.dyuloon.entity.SalesRecordEntity.ShowDayData;
import com.dyuloon.entity.SalesRecordEntity.UserList;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.EstrusrecordService;
import com.dyuloon.service.HealthamalysisService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@RestController
@RequestMapping("/healthamalysis")
public class HealthamalysisController {
    @Autowired
    private HealthamalysisService abnormaldateService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Healthamalysis categorymanagement) {
        boolean save = this.abnormaldateService.save(categorymanagement);
        ResultVO resultVO = save ? ResultVOUtil.success(null, "添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Healthamalysis> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.abnormaldateService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Healthamalysis delegate) {
        boolean update = this.abnormaldateService.updateById(delegate);
        ResultVO resultVO = update ? ResultVOUtil.success(null, "更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id) {
        boolean remove = this.abnormaldateService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null, "删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 可视化折线
    @GetMapping("/getFoldLine")
    public ResultVO queryStore() {
        ShowDayData showDayDataList = new ShowDayData();
        // 获取数据列表
        List<Healthamalysis> healthamalysisList = this.abnormaldateService.list();
        // 遍历数据
        List day = new ArrayList();
        List value = new ArrayList();
        for (int i = 0; i < healthamalysisList.size(); i++) {
            // 塞数据
            day.add(healthamalysisList.get(i).getJibingleixing());
            value.add(Integer.valueOf(healthamalysisList.get(i).getCaicifabing()));
        }
        showDayDataList.setDay(day);
        showDayDataList.setValue(value);
        ResultVO resultVO = ResultVOUtil.success(showDayDataList, "查询成功！");
        return resultVO;
    }

    // 盲乳分析
    @GetMapping("/getOneShow")
    public ResultVO getOneShow() {
        // 最后输出
        List<UserList> finalList = new ArrayList<>();
        // 获取数据列表
        QueryWrapper<Healthamalysis> qw1 = new QueryWrapper<>();
        qw1.lambda().like(Healthamalysis::getJibingleixing, "乳头内陷");
        List<Healthamalysis> healthamalysisList1 = this.abnormaldateService.list(qw1);
        // 遍历数据
        for (int j = 0; j < healthamalysisList1.size(); j++) {
            UserList userList = new UserList();
            userList.setName(healthamalysisList1.get(j).getJibingleixing());
            userList.setValue(Integer.valueOf(healthamalysisList1.get(j).getCaicifabing()));
            finalList.add(userList);
        }
        QueryWrapper<Healthamalysis> qw2 = new QueryWrapper<>();
        qw2.lambda().like(Healthamalysis::getJibingleixing, "假乳");
        List<Healthamalysis> healthamalysisList2 = this.abnormaldateService.list(qw2);
        // 遍历数据
        for (int j = 0; j < healthamalysisList2.size(); j++) {
            UserList userList = new UserList();
            userList.setName(healthamalysisList2.get(j).getJibingleixing());
            userList.setValue(Integer.valueOf(healthamalysisList2.get(j).getCaicifabing()));
            finalList.add(userList);
        }
        QueryWrapper<Healthamalysis> qw3 = new QueryWrapper<>();
        qw3.lambda().like(Healthamalysis::getJibingleixing, "乳房炎");
        List<Healthamalysis> healthamalysisList3 = this.abnormaldateService.list(qw3);
        // 遍历数据
        for (int j = 0; j < healthamalysisList3.size(); j++) {
            UserList userList = new UserList();
            userList.setName(healthamalysisList3.get(j).getJibingleixing());
            userList.setValue(Integer.valueOf(healthamalysisList3.get(j).getCaicifabing()));
            finalList.add(userList);
        }
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }


}

