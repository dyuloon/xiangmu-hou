package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Healthamalysis;
import com.dyuloon.entity.SalesRecordEntity.ShowDayData;
import com.dyuloon.entity.Salesdate;
import com.dyuloon.entity.Scarcerecord;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.SalesdateService;
import com.dyuloon.service.ScarcerecordService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@RestController
@RequestMapping("/scarcerecord")
public class ScarcerecordController {
    @Autowired
    private ScarcerecordService monthlyfeeinputService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Scarcerecord categorymanagement) {
        boolean save = this.monthlyfeeinputService.save(categorymanagement);
        ResultVO resultVO = save ? ResultVOUtil.success(null, "添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Scarcerecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.monthlyfeeinputService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Scarcerecord delegate) {
        boolean update = this.monthlyfeeinputService.updateById(delegate);
        ResultVO resultVO = update ? ResultVOUtil.success(null, "更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id) {
        boolean remove = this.monthlyfeeinputService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null, "删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 产奶趋势
    @GetMapping("/getShowfour")
    public ResultVO showfour() {
        ShowDayData showDayDataList = new ShowDayData();
        // 遍历数据
        String[] day = {"第一季度", "第二季度", "第三季度", "第四季度"};
        // 每一月产奶
        List month = new ArrayList();
        // 算每一年综合
        for (int i = 1; i <= 12; i++) {
            // 得到当前年
            Calendar date = Calendar.getInstance();
            // 拼日期
            String nowData = String.valueOf(date.get(Calendar.YEAR)) + '-' + i;
            // 查每个月
            QueryWrapper<Scarcerecord> qw = new QueryWrapper<>();
            qw.lambda().like(Scarcerecord::getRiqi, nowData);
            List<Scarcerecord> scarcerecordList = this.monthlyfeeinputService.list(qw);
            // 月份之和
            Integer monthSum = 0;
            // 遍历月份
            for (int j = 0; j < scarcerecordList.size(); j++) {
                monthSum = monthSum + Integer.parseInt(scarcerecordList.get(j).getZongnailiang());
            }
            month.add(monthSum);
        }
        // 计算每个季度数据
        Integer sum = 0;
        // 保存季度
        int[] value = new int[4];
        int index = 0;
        // 计算季度
        for (int i = 0; i < month.size(); i++) {
            sum = sum + Integer.parseInt(month.get(i).toString());
            if ((i + 1) % 3 == 0) {
                value[index] = sum;
                sum = 0;
                index++;
            }
        }
        showDayDataList.setDay(Arrays.asList(day));
        showDayDataList.setValue(Collections.singletonList(value));
        ResultVO resultVO = ResultVOUtil.success(showDayDataList, "查询成功！");
        return resultVO;
    }

    // 月度产奶汇总
    @GetMapping("/getShowFive")
    public ResultVO showFive() {
        ShowDayData showDayDataList = new ShowDayData();
        List day = new ArrayList();
        List value = new ArrayList();
        // 遍历十二个月
        for (int i = 1; i <= 12; i++) {
            // 得到当前年
            Calendar date = Calendar.getInstance();
            // 拼日期
            String nowData = String.valueOf(date.get(Calendar.YEAR)) + '-' + i;
            // 查每个月
            QueryWrapper<Scarcerecord> qw = new QueryWrapper<>();
            qw.lambda().like(Scarcerecord::getRiqi, nowData);
            List<Scarcerecord> scarcerecordList = this.monthlyfeeinputService.list(qw);
            // 月份之和
            Integer monthSum = 0;
            // 遍历月份
            for (int j = 0; j < scarcerecordList.size(); j++) {
                monthSum = monthSum + Integer.parseInt(scarcerecordList.get(j).getZongnailiang());
            }
            // 塞进数据
            day.add(nowData);
            value.add(monthSum);
        }
        showDayDataList.setDay(day);
        showDayDataList.setValue(value);
        ResultVO resultVO = ResultVOUtil.success(showDayDataList, "查询成功！");
        return resultVO;
    }

    // 牛只泌乳曲线
    @GetMapping("/getShowOne")
    public ResultVO getShowOne() {
        ShowDayData showDayDataList = new ShowDayData();
        List day = new ArrayList();
        List value = new ArrayList();
        // 遍历十二个月
        for (int i = 1; i <= 12; i++) {
            // 得到当前年
            Calendar date = Calendar.getInstance();
            // 拼日期
            String nowData = String.valueOf(date.get(Calendar.YEAR)) + '-' + i;
            // 查每个月
            QueryWrapper<Scarcerecord> qw = new QueryWrapper<>();
            qw.lambda().like(Scarcerecord::getRiqi, nowData);
            List<Scarcerecord> scarcerecordList = this.monthlyfeeinputService.list(qw);
            // 月份之和
            Integer monthSum = 0;
            // 遍历月份
            for (int j = 0; j < scarcerecordList.size(); j++) {
                monthSum = monthSum + Integer.parseInt(scarcerecordList.get(j).getZongnailiang());
            }
            // 塞进数据
            day.add(nowData);
            value.add(monthSum);
        }
        showDayDataList.setDay(day);
        showDayDataList.setValue(value);
        ResultVO resultVO = ResultVOUtil.success(showDayDataList, "查询成功！");
        return resultVO;
    }


}

