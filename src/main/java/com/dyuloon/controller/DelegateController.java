package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Categorymanagement;
import com.dyuloon.entity.Delegate;
import com.dyuloon.entity.SalesRecordEntity.UserList;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.CategorymanagementService;
import com.dyuloon.service.DelegateService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@RestController
@RequestMapping("/delegate")
public class DelegateController {

    @Autowired
    private DelegateService categorymanagementService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Delegate categorymanagement) {
        boolean save = this.categorymanagementService.save(categorymanagement);
        ResultVO resultVO = save ? ResultVOUtil.success(null, "添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Delegate> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.categorymanagementService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Delegate delegate) {
        boolean update = this.categorymanagementService.updateById(delegate);
        ResultVO resultVO = update ? ResultVOUtil.success(null, "更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id) {
        boolean remove = this.categorymanagementService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null, "删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 受孕率统计
    @GetMapping("/thirdShow")
    public ResultVO thirdShow() {
        String[] day = {"第一季度", "第二季度", "第三季度", "第四季度"};
        // 得到当前年
        Calendar date = Calendar.getInstance();
        List month = new ArrayList<>();
        // 遍历十二个月
        for (int i = 1; i < 13; i++) {
            // 拼日期
            String nowData = String.valueOf(date.get(Calendar.YEAR)) + '-' + i;
            QueryWrapper<Delegate> qw = new QueryWrapper<>();
            qw.lambda().like(Delegate::getPeizhong, nowData);
            int num = this.categorymanagementService.count(qw);
            month.add(num);
        }
        // 计算每个季度数据
        Integer sum = 0;
        // 保存季度
        int[] value = new int[4];
        int index = 0;
        // 计算季度
        for (int i = 0; i < month.size(); i++) {
            sum = sum + Integer.parseInt(month.get(i).toString());
            if ((i + 1) % 3 == 0) {
                value[index] = sum;
                sum = 0;
                index++;
            }
        }
        List<UserList> finalList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            UserList userList = new UserList();
            userList.setValue(value[i]);
            userList.setName(day[i]);
            finalList.add(userList);
        }
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }

    // 受胎率汇总

}

