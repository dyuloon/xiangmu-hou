package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Cashcost;
import com.dyuloon.entity.Decisionsupport;
import com.dyuloon.entity.SalesRecordEntity.UserList;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.CashcostService;
import com.dyuloon.service.DecisionsupportService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@RestController
@RequestMapping("/decisionsupport")
public class DecisionsupportController {
    @Autowired
    private DecisionsupportService abnormaldateService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Decisionsupport categorymanagement) {
        boolean save = this.abnormaldateService.save(categorymanagement);
        ResultVO resultVO = save ? ResultVOUtil.success(null,"添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Decisionsupport> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.abnormaldateService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Decisionsupport delegate){
        boolean update = this.abnormaldateService.updateById(delegate);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id){
        boolean remove = this.abnormaldateService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null,"删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 数据统计
    @GetMapping("/firstShow")
    public ResultVO deleteStore(){
        // 最终输出
        List<UserList> finalList = new ArrayList<>();
        // 查所有数据
        List<Decisionsupport> decisionsupportList = this.abnormaldateService.list();
        // 遍历所有数据
        for (int i = 0; i < decisionsupportList.size(); i++) {
            UserList userList = new UserList();
            userList.setName(decisionsupportList.get(i).getJibingleix());
            userList.setValue(Integer.valueOf(decisionsupportList.get(i).getFabingzongshu()));
            finalList.add(userList);
        }
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }


}

