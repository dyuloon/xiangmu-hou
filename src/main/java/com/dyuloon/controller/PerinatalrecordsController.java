package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Delegate;
import com.dyuloon.entity.Perinatalrecords;
import com.dyuloon.entity.SalesRecordEntity.ShowDayData;
import com.dyuloon.entity.Storehousemanagement;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.PerinatalrecordsService;
import com.dyuloon.service.StorehousemanagementService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@RestController
@RequestMapping("/perinatalrecords")
public class PerinatalrecordsController {

    @Autowired
    private PerinatalrecordsService nowService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Perinatalrecords nowEntity) {
        boolean save = this.nowService.save(nowEntity);
        ResultVO resultVO = save ? ResultVOUtil.success(null,"添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Perinatalrecords nowEntity){
        boolean update = this.nowService.updateById(nowEntity);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Perinatalrecords> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.nowService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id){
        boolean remove = this.nowService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null,"删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 年度产犊汇总
    @GetMapping("/twoShow")
    public ResultVO thirdShow() {
        // 最终输出
        ShowDayData finalList = new ShowDayData();
        List day = new ArrayList();
        day.add("第一季度");
        day.add("第二季度");
        day.add("第三季度");
        day.add("第四季度");
        // 得到当前年
        Calendar date = Calendar.getInstance();
        List month = new ArrayList<>();
        // 遍历十二个月
        for (int i = 1; i < 13; i++) {
            // 拼日期
            String nowData = String.valueOf(date.get(Calendar.YEAR)) + '-' + i;
            QueryWrapper<Perinatalrecords> qw = new QueryWrapper<>();
            qw.lambda().like(Perinatalrecords::getZhuansheriqi, nowData);
            int num = this.nowService.count(qw);
            month.add(num);
        }
        System.out.println(month);
        // 计算每个季度数据
        Integer sum = 0;
        // 保存季度
        List value = new ArrayList();
        int index = 0;
        // 计算季度
        for (int i = 0; i < month.size(); i++) {
            sum = sum + Integer.parseInt(month.get(i).toString());
            if ((i + 1) % 3 == 0) {
                value.add(sum);
                sum = 0;
                index++;
            }
        }
        finalList.setDay(day);
        finalList.setValue(value);
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }
}

