package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.SalesRecordEntity.UserList;
import com.dyuloon.entity.StoreStoremanage;
import com.dyuloon.entity.Storemanagement;
import com.dyuloon.entity.Unitmanagement;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.StoreStoremanageService;
import com.dyuloon.service.StoremanagementService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@RestController
@RequestMapping("/storemanagement")
public class StoremanagementController {

    @Autowired
    private StoremanagementService storeStoremanageService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Storemanagement storeStoremanage) {
        boolean save = this.storeStoremanageService.save(storeStoremanage);
        ResultVO resultVO = save ? ResultVOUtil.success(null,"添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Storemanagement> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.storeStoremanageService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }
    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Storemanagement storeStoremanage){
        boolean update = this.storeStoremanageService.updateById(storeStoremanage);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id){
        boolean remove = this.storeStoremanageService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null,"删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 繁殖分析
    @GetMapping("/firstShow")
    public ResultVO firstShow(){
        List<UserList> finalList = new ArrayList<>();
        List<Storemanagement> storemanagementList = this.storeStoremanageService.list();
        // 遍历
        for (int i = 0; i < storemanagementList.size(); i++) {
            UserList userList = new UserList();
            userList.setValue(Integer.valueOf(storemanagementList.get(i).getNiuzhishu()));
            userList.setName(storemanagementList.get(i).getLeixing());
            finalList.add(userList);
        }
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }
}

