package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Individualmilkproduction;
import com.dyuloon.entity.SalesRecordEntity.UserList;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.IndividualmilkproductionService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@RestController
@RequestMapping("/individualmilkproduction")
public class IndividualmilkproductionController {
    @Autowired
    private IndividualmilkproductionService individualmilkproductionService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Individualmilkproduction individualmilkproduction) {
        boolean save = this.individualmilkproductionService.save(individualmilkproduction);
        ResultVO resultVO = save ? ResultVOUtil.success(null,"添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Individualmilkproduction> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.individualmilkproductionService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Individualmilkproduction individualmilkproduction){
        boolean update = this.individualmilkproductionService.updateById(individualmilkproduction);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id){
        boolean remove = this.individualmilkproductionService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null,"删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }

    // 各舍每日奶量
    @GetMapping("/thirdShow")
    public ResultVO thirdShow(){
        List<UserList> finalList = new ArrayList<>();
        // 查找所有牛舍编号
        QueryWrapper<Individualmilkproduction> qw = new QueryWrapper<>();
        qw.select("Distinct niushebianhao");
        List<Individualmilkproduction> list01 = this.individualmilkproductionService.list(qw);
        // 遍历牛舍
        for (int i = 0; i < list01.size(); i++) {
            UserList userList = new UserList();
            QueryWrapper<Individualmilkproduction> qw1 = new QueryWrapper<>();
            qw1.lambda().eq(Individualmilkproduction::getNiushebianhao,list01.get(i).getNiushebianhao());
            // 查单一牛舍总和
            List<Individualmilkproduction> list02 = this.individualmilkproductionService.list(qw1);
            int sum = 0;
            for (int j = 0; j < list02.size(); j++) {
                sum = sum + Integer.parseInt(list02.get(j).getDangtiannailiang());
            }
            userList.setValue(sum);
            userList.setName(list01.get(i).getNiushebianhao());
            finalList.add(userList);
        }
        ResultVO resultVO = ResultVOUtil.success(finalList, "查询成功！");
        return resultVO;
    }
}

