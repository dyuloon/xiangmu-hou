package com.dyuloon.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dyuloon.entity.Categorymanagement;
import com.dyuloon.entity.Unitmanagement;
import com.dyuloon.from.SearchForm;
import com.dyuloon.service.CategorymanagementService;
import com.dyuloon.service.UnitmanagementService;
import com.dyuloon.util.PageVOUtil;
import com.dyuloon.util.ResultVOUtil;
import com.dyuloon.vo.PageVO;
import com.dyuloon.vo.ResultVO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@RestController
@RequestMapping("/unitmanagement")
public class UnitmanagementController {

    @Autowired
    private UnitmanagementService unitmanagementService;

    // 添加店铺
    @PostMapping
    public ResultVO saveStore(@RequestBody Unitmanagement unitmanagement) {
        boolean save = this.unitmanagementService.save(unitmanagement);
        ResultVO resultVO = save ? ResultVOUtil.success(null,"添加成功！") : ResultVOUtil.fail("添加失败！");
        return resultVO;
    }

    // 查询店铺
    @GetMapping
    public PageVO queryStore(SearchForm searchForm) {
        Page storePage = new Page(searchForm.getPageNum(), searchForm.getPageSize());
        QueryWrapper<Unitmanagement> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(searchForm.getValue()), searchForm.getKey(), searchForm.getValue());
        this.unitmanagementService.pageMaps(storePage, queryWrapper.orderByDesc("id"));
        PageVO pageVO = PageVOUtil.success(storePage.getRecords(), "查询成功！", storePage.getTotal(), storePage.getCurrent(), storePage.getSize());
        return pageVO;
    }

    // 修改店铺
    @PutMapping
    public ResultVO updateStore(@RequestBody Unitmanagement unitmanagement){
        boolean update = this.unitmanagementService.updateById(unitmanagement);
        ResultVO resultVO = update ? ResultVOUtil.success(null,"更新成功！") : ResultVOUtil.fail("更新失败！");
        return resultVO;
    }

    // 删除店铺
    @DeleteMapping("{id}")
    public ResultVO deleteStore(@PathVariable Integer id){
        boolean remove = this.unitmanagementService.removeById(id);
        ResultVO resultVO = remove ? ResultVOUtil.success(null,"删除成功！") : ResultVOUtil.fail("删除失败！");
        return resultVO;
    }
}

