package com.dyuloon.service;

import com.dyuloon.entity.Goodsmanagement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
public interface GoodsmanagementService extends IService<Goodsmanagement> {

}
