package com.dyuloon.service;

import com.dyuloon.entity.Storemanagement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
public interface StoremanagementService extends IService<Storemanagement> {

}
