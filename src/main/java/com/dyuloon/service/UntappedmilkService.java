package com.dyuloon.service;

import com.dyuloon.entity.Untappedmilk;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
public interface UntappedmilkService extends IService<Untappedmilk> {

}
