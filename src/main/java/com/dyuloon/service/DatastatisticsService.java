package com.dyuloon.service;

import com.dyuloon.entity.Datastatistics;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
public interface DatastatisticsService extends IService<Datastatistics> {

}
