package com.dyuloon.service;

import com.dyuloon.entity.Delegate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
public interface DelegateService extends IService<Delegate> {

}
