package com.dyuloon.service.impl;

import com.dyuloon.entity.Bullasset;
import com.dyuloon.mapper.BullassetMapper;
import com.dyuloon.service.BullassetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class BullassetServiceImpl extends ServiceImpl<BullassetMapper, Bullasset> implements BullassetService {

}
