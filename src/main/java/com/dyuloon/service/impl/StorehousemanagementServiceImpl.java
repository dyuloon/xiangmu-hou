package com.dyuloon.service.impl;

import com.dyuloon.entity.Storehousemanagement;
import com.dyuloon.mapper.StorehousemanagementMapper;
import com.dyuloon.service.StorehousemanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class StorehousemanagementServiceImpl extends ServiceImpl<StorehousemanagementMapper, Storehousemanagement> implements StorehousemanagementService {

}
