package com.dyuloon.service.impl;

import com.dyuloon.entity.Transportationmanagement;
import com.dyuloon.mapper.TransportationmanagementMapper;
import com.dyuloon.service.TransportationmanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class TransportationmanagementServiceImpl extends ServiceImpl<TransportationmanagementMapper, Transportationmanagement> implements TransportationmanagementService {

}
