package com.dyuloon.service.impl;

import com.dyuloon.entity.Empployeedate;
import com.dyuloon.mapper.EmpployeedateMapper;
import com.dyuloon.service.EmpployeedateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class EmpployeedateServiceImpl extends ServiceImpl<EmpployeedateMapper, Empployeedate> implements EmpployeedateService {

}
