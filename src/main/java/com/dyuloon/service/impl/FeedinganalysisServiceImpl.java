package com.dyuloon.service.impl;

import com.dyuloon.entity.Feedinganalysis;
import com.dyuloon.mapper.FeedinganalysisMapper;
import com.dyuloon.service.FeedinganalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Service
public class FeedinganalysisServiceImpl extends ServiceImpl<FeedinganalysisMapper, Feedinganalysis> implements FeedinganalysisService {

}
