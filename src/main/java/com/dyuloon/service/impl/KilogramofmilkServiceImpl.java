package com.dyuloon.service.impl;

import com.dyuloon.entity.Kilogramofmilk;
import com.dyuloon.mapper.KilogramofmilkMapper;
import com.dyuloon.service.KilogramofmilkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class KilogramofmilkServiceImpl extends ServiceImpl<KilogramofmilkMapper, Kilogramofmilk> implements KilogramofmilkService {

}
