package com.dyuloon.service.impl;

import com.dyuloon.entity.Salesrecordother;
import com.dyuloon.mapper.SalesrecordotherMapper;
import com.dyuloon.service.SalesrecordotherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Service
public class SalesrecordotherServiceImpl extends ServiceImpl<SalesrecordotherMapper, Salesrecordother> implements SalesrecordotherService {

}
