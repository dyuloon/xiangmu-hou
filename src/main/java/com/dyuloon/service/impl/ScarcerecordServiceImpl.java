package com.dyuloon.service.impl;

import com.dyuloon.entity.Scarcerecord;
import com.dyuloon.mapper.ScarcerecordMapper;
import com.dyuloon.service.ScarcerecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class ScarcerecordServiceImpl extends ServiceImpl<ScarcerecordMapper, Scarcerecord> implements ScarcerecordService {

}
