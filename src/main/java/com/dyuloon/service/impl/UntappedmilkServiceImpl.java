package com.dyuloon.service.impl;

import com.dyuloon.entity.Untappedmilk;
import com.dyuloon.mapper.UntappedmilkMapper;
import com.dyuloon.service.UntappedmilkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class UntappedmilkServiceImpl extends ServiceImpl<UntappedmilkMapper, Untappedmilk> implements UntappedmilkService {

}
