package com.dyuloon.service.impl;

import com.dyuloon.entity.Materialmanagement;
import com.dyuloon.mapper.MaterialmanagementMapper;
import com.dyuloon.service.MaterialmanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class MaterialmanagementServiceImpl extends ServiceImpl<MaterialmanagementMapper, Materialmanagement> implements MaterialmanagementService {

}
