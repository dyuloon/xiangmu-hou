package com.dyuloon.service.impl;

import com.dyuloon.entity.Estrusrecord;
import com.dyuloon.mapper.EstrusrecordMapper;
import com.dyuloon.service.EstrusrecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class EstrusrecordServiceImpl extends ServiceImpl<EstrusrecordMapper, Estrusrecord> implements EstrusrecordService {

}
