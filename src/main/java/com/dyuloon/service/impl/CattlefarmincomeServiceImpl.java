package com.dyuloon.service.impl;

import com.dyuloon.entity.Cattlefarmincome;
import com.dyuloon.mapper.CattlefarmincomeMapper;
import com.dyuloon.service.CattlefarmincomeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Service
public class CattlefarmincomeServiceImpl extends ServiceImpl<CattlefarmincomeMapper, Cattlefarmincome> implements CattlefarmincomeService {

}
