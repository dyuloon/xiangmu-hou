package com.dyuloon.service.impl;

import com.dyuloon.entity.Healthamalysis;
import com.dyuloon.mapper.HealthamalysisMapper;
import com.dyuloon.service.HealthamalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class HealthamalysisServiceImpl extends ServiceImpl<HealthamalysisMapper, Healthamalysis> implements HealthamalysisService {

}
