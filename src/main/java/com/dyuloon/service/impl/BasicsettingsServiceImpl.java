package com.dyuloon.service.impl;

import com.dyuloon.entity.Basicsettings;
import com.dyuloon.mapper.BasicsettingsMapper;
import com.dyuloon.service.BasicsettingsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class BasicsettingsServiceImpl extends ServiceImpl<BasicsettingsMapper, Basicsettings> implements BasicsettingsService {

}
