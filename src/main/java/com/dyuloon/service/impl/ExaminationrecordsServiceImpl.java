package com.dyuloon.service.impl;

import com.dyuloon.entity.Examinationrecords;
import com.dyuloon.mapper.ExaminationrecordsMapper;
import com.dyuloon.service.ExaminationrecordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class ExaminationrecordsServiceImpl extends ServiceImpl<ExaminationrecordsMapper, Examinationrecords> implements ExaminationrecordsService {

}
