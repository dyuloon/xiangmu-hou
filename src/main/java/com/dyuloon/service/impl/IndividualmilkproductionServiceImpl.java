package com.dyuloon.service.impl;

import com.dyuloon.entity.Individualmilkproduction;
import com.dyuloon.mapper.IndividualmilkproductionMapper;
import com.dyuloon.service.IndividualmilkproductionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class IndividualmilkproductionServiceImpl extends ServiceImpl<IndividualmilkproductionMapper, Individualmilkproduction> implements IndividualmilkproductionService {

}
