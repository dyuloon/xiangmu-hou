package com.dyuloon.service.impl;

import com.dyuloon.entity.Perinatalrecords;
import com.dyuloon.mapper.PerinatalrecordsMapper;
import com.dyuloon.service.PerinatalrecordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class PerinatalrecordsServiceImpl extends ServiceImpl<PerinatalrecordsMapper, Perinatalrecords> implements PerinatalrecordsService {

}
