package com.dyuloon.service.impl;

import com.dyuloon.entity.Cattlefarmexpenditure;
import com.dyuloon.mapper.CattlefarmexpenditureMapper;
import com.dyuloon.service.CattlefarmexpenditureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Service
public class CattlefarmexpenditureServiceImpl extends ServiceImpl<CattlefarmexpenditureMapper, Cattlefarmexpenditure> implements CattlefarmexpenditureService {

}
