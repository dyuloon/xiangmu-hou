package com.dyuloon.service.impl;

import com.dyuloon.entity.Shiftmanagement;
import com.dyuloon.mapper.ShiftmanagementMapper;
import com.dyuloon.service.ShiftmanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Service
public class ShiftmanagementServiceImpl extends ServiceImpl<ShiftmanagementMapper, Shiftmanagement> implements ShiftmanagementService {

}
