package com.dyuloon.service.impl;

import com.dyuloon.entity.Farmprofitanalysis;
import com.dyuloon.mapper.FarmprofitanalysisMapper;
import com.dyuloon.service.FarmprofitanalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Service
public class FarmprofitanalysisServiceImpl extends ServiceImpl<FarmprofitanalysisMapper, Farmprofitanalysis> implements FarmprofitanalysisService {

}
