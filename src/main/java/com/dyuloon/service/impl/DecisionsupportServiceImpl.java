package com.dyuloon.service.impl;

import com.dyuloon.entity.Decisionsupport;
import com.dyuloon.mapper.DecisionsupportMapper;
import com.dyuloon.service.DecisionsupportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class DecisionsupportServiceImpl extends ServiceImpl<DecisionsupportMapper, Decisionsupport> implements DecisionsupportService {

}
