package com.dyuloon.service.impl;

import com.dyuloon.entity.Inboundoutbound;
import com.dyuloon.mapper.InboundoutboundMapper;
import com.dyuloon.service.InboundoutboundService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class InboundoutboundServiceImpl extends ServiceImpl<InboundoutboundMapper, Inboundoutbound> implements InboundoutboundService {

}
