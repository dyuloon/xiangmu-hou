package com.dyuloon.service.impl;

import com.dyuloon.entity.Employeemanagement;
import com.dyuloon.mapper.EmployeemanagementMapper;
import com.dyuloon.service.EmployeemanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class EmployeemanagementServiceImpl extends ServiceImpl<EmployeemanagementMapper, Employeemanagement> implements EmployeemanagementService {

}
