package com.dyuloon.service.impl;

import com.dyuloon.entity.Delegate;
import com.dyuloon.mapper.DelegateMapper;
import com.dyuloon.service.DelegateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class DelegateServiceImpl extends ServiceImpl<DelegateMapper, Delegate> implements DelegateService {

}
