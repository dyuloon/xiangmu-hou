package com.dyuloon.service.impl;

import com.dyuloon.entity.Monthlyfeeinput;
import com.dyuloon.mapper.MonthlyfeeinputMapper;
import com.dyuloon.service.MonthlyfeeinputService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class MonthlyfeeinputServiceImpl extends ServiceImpl<MonthlyfeeinputMapper, Monthlyfeeinput> implements MonthlyfeeinputService {

}
