package com.dyuloon.service.impl;

import com.dyuloon.entity.Storemanagement;
import com.dyuloon.mapper.StoremanagementMapper;
import com.dyuloon.service.StoremanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-14
 */
@Service
public class StoremanagementServiceImpl extends ServiceImpl<StoremanagementMapper, Storemanagement> implements StoremanagementService {

}
