package com.dyuloon.service.impl;

import com.dyuloon.entity.Marrecords;
import com.dyuloon.mapper.MarrecordsMapper;
import com.dyuloon.service.MarrecordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class MarrecordsServiceImpl extends ServiceImpl<MarrecordsMapper, Marrecords> implements MarrecordsService {

}
