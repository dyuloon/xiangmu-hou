package com.dyuloon.service.impl;

import com.dyuloon.entity.Salesdate;
import com.dyuloon.mapper.SalesdateMapper;
import com.dyuloon.service.SalesdateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class SalesdateServiceImpl extends ServiceImpl<SalesdateMapper, Salesdate> implements SalesdateService {

}
