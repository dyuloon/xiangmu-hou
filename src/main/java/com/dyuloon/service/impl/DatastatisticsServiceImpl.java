package com.dyuloon.service.impl;

import com.dyuloon.entity.Datastatistics;
import com.dyuloon.mapper.DatastatisticsMapper;
import com.dyuloon.service.DatastatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-15
 */
@Service
public class DatastatisticsServiceImpl extends ServiceImpl<DatastatisticsMapper, Datastatistics> implements DatastatisticsService {

}
