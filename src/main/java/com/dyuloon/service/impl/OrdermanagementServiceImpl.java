package com.dyuloon.service.impl;

import com.dyuloon.entity.Ordermanagement;
import com.dyuloon.mapper.OrdermanagementMapper;
import com.dyuloon.service.OrdermanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class OrdermanagementServiceImpl extends ServiceImpl<OrdermanagementMapper, Ordermanagement> implements OrdermanagementService {

}
