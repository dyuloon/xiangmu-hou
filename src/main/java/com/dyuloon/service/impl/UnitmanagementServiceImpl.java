package com.dyuloon.service.impl;

import com.dyuloon.entity.Unitmanagement;
import com.dyuloon.mapper.UnitmanagementMapper;
import com.dyuloon.service.UnitmanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class UnitmanagementServiceImpl extends ServiceImpl<UnitmanagementMapper, Unitmanagement> implements UnitmanagementService {

}
