package com.dyuloon.service.impl;

import com.dyuloon.entity.Estrouscow;
import com.dyuloon.mapper.EstrouscowMapper;
import com.dyuloon.service.EstrouscowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class EstrouscowServiceImpl extends ServiceImpl<EstrouscowMapper, Estrouscow> implements EstrouscowService {

}
