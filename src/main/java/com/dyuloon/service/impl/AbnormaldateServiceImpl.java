package com.dyuloon.service.impl;

import com.dyuloon.entity.Abnormaldate;
import com.dyuloon.mapper.AbnormaldateMapper;
import com.dyuloon.service.AbnormaldateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class AbnormaldateServiceImpl extends ServiceImpl<AbnormaldateMapper, Abnormaldate> implements AbnormaldateService {

}
