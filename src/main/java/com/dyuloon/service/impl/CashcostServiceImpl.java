package com.dyuloon.service.impl;

import com.dyuloon.entity.Cashcost;
import com.dyuloon.mapper.CashcostMapper;
import com.dyuloon.service.CashcostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-11
 */
@Service
public class CashcostServiceImpl extends ServiceImpl<CashcostMapper, Cashcost> implements CashcostService {

}
