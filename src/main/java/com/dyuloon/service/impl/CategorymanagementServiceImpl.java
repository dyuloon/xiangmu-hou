package com.dyuloon.service.impl;

import com.dyuloon.entity.Categorymanagement;
import com.dyuloon.mapper.CategorymanagementMapper;
import com.dyuloon.service.CategorymanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class CategorymanagementServiceImpl extends ServiceImpl<CategorymanagementMapper, Categorymanagement> implements CategorymanagementService {

}
