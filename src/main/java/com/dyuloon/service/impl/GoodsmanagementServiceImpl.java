package com.dyuloon.service.impl;

import com.dyuloon.entity.Goodsmanagement;
import com.dyuloon.mapper.GoodsmanagementMapper;
import com.dyuloon.service.GoodsmanagementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dyuloon
 * @since 2023-05-10
 */
@Service
public class GoodsmanagementServiceImpl extends ServiceImpl<GoodsmanagementMapper, Goodsmanagement> implements GoodsmanagementService {

}
