package com.dyuloon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.dyuloon.mapper")
public class SalesmsSpringbootApplication {

    public static void main(String[] args) {
        // 热部署启停
        System.setProperty("spring.devtools.restart.enabled","true");
        SpringApplication.run(SalesmsSpringbootApplication.class, args);
    }

}
