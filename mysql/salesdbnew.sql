/*
 Navicat Premium Data Transfer

 Source Server         : phpStudyMySql
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : localhost:3306
 Source Schema         : salesdbnew

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 22/11/2023 11:23:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for abnormaldate
-- ----------------------------
DROP TABLE IF EXISTS `abnormaldate`;
CREATE TABLE `abnormaldate`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dianzibiaoqian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shuoming` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of abnormaldate
-- ----------------------------
INSERT INTO `abnormaldate` VALUES (1, '1-120', '20', '超过12小时未上报记录');
INSERT INTO `abnormaldate` VALUES (2, '1-121', '21', '超过5小时未上报记录');
INSERT INTO `abnormaldate` VALUES (3, '1-122', '22', '超过2小时未上报记录');
INSERT INTO `abnormaldate` VALUES (4, '1-123', '23', '超过10小时未上报记录');
INSERT INTO `abnormaldate` VALUES (5, '1-124', '24', '超过8小时未上报记录');

-- ----------------------------
-- Table structure for basicsettings
-- ----------------------------
DROP TABLE IF EXISTS `basicsettings`;
CREATE TABLE `basicsettings`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `jingyemingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gongniubianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyinjianma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gouxiaoshangmingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gouxiangshangbianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gouxiangshangwuzi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lianxiren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dianhua` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of basicsettings
-- ----------------------------
INSERT INTO `basicsettings` VALUES (1, '优质精液', '1-110', 'yz', 'XXX购销商', '3-110', '饲料', '张某某', '13524879852');
INSERT INTO `basicsettings` VALUES (2, '良质精液', '1-111', 'lz', 'XXX购销商', '3-111', '药品', '王某某', '13845684777');
INSERT INTO `basicsettings` VALUES (3, '优质精液', '1-112', 'yz', 'XXX购销商', '3-112', '耗材', '李某某', '15611475236');
INSERT INTO `basicsettings` VALUES (4, '三等精液', '1-113', 'sd', 'XXX购销商', '3-113', '药品', '段某某', '14478523685');
INSERT INTO `basicsettings` VALUES (5, '优质精液', '1-114', 'yz', 'XXX购销商', '3-114', '饲料', '郭某某', '13547852685');
INSERT INTO `basicsettings` VALUES (6, NULL, NULL, NULL, NULL, '', NULL, NULL, '');

-- ----------------------------
-- Table structure for bullasset
-- ----------------------------
DROP TABLE IF EXISTS `bullasset`;
CREATE TABLE `bullasset`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `dangqianzichan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niushe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuqunzhuangtai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fanzhizhuangtai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bullasset
-- ----------------------------
INSERT INTO `bullasset` VALUES (1, '18万', '1号牛舍', '正常', '正常');
INSERT INTO `bullasset` VALUES (2, '20万', '2号牛舍', '正常', '正常');
INSERT INTO `bullasset` VALUES (3, '35万', '3号牛舍', '正常', '正常');
INSERT INTO `bullasset` VALUES (4, '14万', '4号牛舍', '正常', '正常');

-- ----------------------------
-- Table structure for cashcost
-- ----------------------------
DROP TABLE IF EXISTS `cashcost`;
CREATE TABLE `cashcost`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `zongshouru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zongzhichu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lirun` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhanbi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cashcost
-- ----------------------------
INSERT INTO `cashcost` VALUES (1, '1003万', '500万', '500万', '50%');

-- ----------------------------
-- Table structure for categorymanagement
-- ----------------------------
DROP TABLE IF EXISTS `categorymanagement`;
CREATE TABLE `categorymanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `mingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `leibie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jianzhumianji` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuzhiishuliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niushejingjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of categorymanagement
-- ----------------------------
INSERT INTO `categorymanagement` VALUES (1, '1号牛舍', '2-101', '大牛舍', '120', '32', '32');
INSERT INTO `categorymanagement` VALUES (2, '2号牛舍', '2-102', '大牛舍', '120', '10', '10');
INSERT INTO `categorymanagement` VALUES (3, '3号牛舍', '2-103', '小牛舍', '60', '16', '18');
INSERT INTO `categorymanagement` VALUES (4, '4号牛舍', '2-104', '大牛舍', '120', '10', '10');
INSERT INTO `categorymanagement` VALUES (5, '5号牛舍', '2-105', '小牛舍', '60', '16', '16');

-- ----------------------------
-- Table structure for cattlefarmexpenditure
-- ----------------------------
DROP TABLE IF EXISTS `cattlefarmexpenditure`;
CREATE TABLE `cattlefarmexpenditure`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `duniutourichengben` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `duniuzongjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yuchengniutourichengben` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yuchengniuzongjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qingnianniutourichengben` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qingnianniuzongjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `miruniutourichengbne` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `miruniuzongjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gannainiutourichengben` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gannainiuzongjia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cattlefarmexpenditure
-- ----------------------------
INSERT INTO `cattlefarmexpenditure` VALUES (1, '1520', '2501', '15852', '1548', '51658', '54684', '5648', '5646', '1425', '21186');
INSERT INTO `cattlefarmexpenditure` VALUES (2, '1758', '5558', '20140', '153', '4561', '5465', '48', '52156', '484532', '1654');
INSERT INTO `cattlefarmexpenditure` VALUES (3, '1251', '1520', '156', '48', '4564', '98465', '184', '561', '8481', '6518');

-- ----------------------------
-- Table structure for cattlefarmincome
-- ----------------------------
DROP TABLE IF EXISTS `cattlefarmincome`;
CREATE TABLE `cattlefarmincome`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `riqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `azhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ajiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bzhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bjiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `czhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cjiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dzhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `djiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ezhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ejiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fzhongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fjiage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cattlefarmincome
-- ----------------------------
INSERT INTO `cattlefarmincome` VALUES (1, '2023-5-6', '53吨', '15283元', '53吨', '15283元', '53吨', '15283元', '53吨', '15283元', '53吨', '15283元', '53吨', '15283元');
INSERT INTO `cattlefarmincome` VALUES (2, '2023-5-7', '44吨', '16548元', '44吨', '16548元', '44吨', '16548元', '44吨', '16548元', '44吨', '16548元', '44吨', '16548元');
INSERT INTO `cattlefarmincome` VALUES (3, '2023-5-8', '35吨', '1568元', '35吨', '1568元', '35吨', '1568元', '35吨', '1568元', '35吨', '1568元', '35吨', '1568元');
INSERT INTO `cattlefarmincome` VALUES (4, '2023-5-9', '26吨', '156186元', '26吨', '156186元', '26吨', '156186元', '26吨', '156186元', '26吨', '156186元', '26吨', '156186元');
INSERT INTO `cattlefarmincome` VALUES (5, '2023-5-10', '53吨', '4864元', '52吨', '4864元', '52吨', '4864元', '52吨', '4864元', '52吨', '4864元', '52吨', '4864元');

-- ----------------------------
-- Table structure for datastatistics
-- ----------------------------
DROP TABLE IF EXISTS `datastatistics`;
CREATE TABLE `datastatistics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `xiangmu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yiyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `eryue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sanyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wuyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `liuyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qiyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bayue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jiuyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shiyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shiyiyue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shieryue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `heji` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yijidu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `erjidu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sanjidu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sijidu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datastatistics
-- ----------------------------
INSERT INTO `datastatistics` VALUES (1, '单产目标', '156', '48456', '89456', '16', '4865', '165', '46', '515', '8645', '6165', '465', '456', '1654', '654', '561', '561', '654');
INSERT INTO `datastatistics` VALUES (2, '实际单产', '456', '456', '156', '132', '6', '48', '94', '5', '61', '864', '4', '56', '84', '654', '65498', '456', '1');
INSERT INTO `datastatistics` VALUES (3, '泌乳牛月饲养头日', '456', '13', '484', '56489', '45', '618', '945', '61', '648', '6', '51', '84', '645', '1', '684', '84', '561');
INSERT INTO `datastatistics` VALUES (6, '总奶量', '156', '156', '48', '45', '1', '848', '1', '651', '6', '84', '46', '5', '1', '65', '484', '65', '4');
INSERT INTO `datastatistics` VALUES (4, '单产目标', '15', '61', '684', '84', '65', '456', '16', '848', '4', '561', '65', '48', '49', '4', '564', '849', '456');

-- ----------------------------
-- Table structure for decisionsupport
-- ----------------------------
DROP TABLE IF EXISTS `decisionsupport`;
CREATE TABLE `decisionsupport`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `jibingleix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yangbenzongshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fabingzongshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fabinglv` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fanzhili` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `diyicichandu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingzhouqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shujingcishu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of decisionsupport
-- ----------------------------
INSERT INTO `decisionsupport` VALUES (1, '乳房炎', '500', '25', '5%', '优秀', '一岁', '18天', '1');
INSERT INTO `decisionsupport` VALUES (2, '产后瘫痪', '500', '15', '3%', '良好', '两岁', '17天', '2');
INSERT INTO `decisionsupport` VALUES (3, '胎衣不下', '500', '10', '2%', '优秀', '一岁半', '21天', '1');
INSERT INTO `decisionsupport` VALUES (4, '奶牛不孕', '500', '12', '2.4%', '优秀', '两岁半', '19天', '1');
INSERT INTO `decisionsupport` VALUES (5, '酮病', '501', '17', '3.4%', '良好', '一岁半', '21天', '4');

-- ----------------------------
-- Table structure for delegate
-- ----------------------------
DROP TABLE IF EXISTS `delegate`;
CREATE TABLE `delegate`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuchang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taici` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peizhong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingleib` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faxianren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shifoupeizhong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weipeiyuanyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gongniuhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dongjinghao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shifouxingkong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peizhhongyuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peizhongshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shangcifanzhi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of delegate
-- ----------------------------
INSERT INTO `delegate` VALUES (1, '186427', '320000186427562', '演示牧场', '3', '2023-1-25', '2020-11-25', '自然发情', '员工1', '否', NULL, '3111101884', '3111101884', '否', '小王', '178', '好');
INSERT INTO `delegate` VALUES (2, '186425', '320000186423547', '演示牧场', '4', '2023-1-25', '2020-11-26', '自然发情', '员工2', '否', NULL, '3111101889', '3111101821', '否', '小李', '116', '好');
INSERT INTO `delegate` VALUES (3, '186497', '3200001864275542', '演示牧场', '3', '2023-2-25', '2020-11-29', '自然发情', '员工3', '否', NULL, '3111101881', '3111101889', '否', '小王', '198', '好');
INSERT INTO `delegate` VALUES (4, '186428', '320000186427592', '演示牧场', '3', '2023-3-25', '2020-10-25', '自然发情', '员工1', '否', NULL, '311110189', '3111101884', '否', '小张', '119', '好');
INSERT INTO `delegate` VALUES (5, '186418', '320000186427519', '演示牧场', '6', '2023-4-25', '2020-11-30', '自然发情', '员工9', '否', NULL, '311110168', '3111101888', '否', '小零', '190', '好');
INSERT INTO `delegate` VALUES (6, '186564', '32000018234896', '演示牧场', '15', '2023-5-25', '2022-11-25', '自然发情', '员工10', '否', NULL, '311134645', '545475845', '否', '小王', '178', '好');
INSERT INTO `delegate` VALUES (7, '186546', '320002564825469', '演示牧场', '9', '2023-5-25', '2021-10-05', '自然发情', '员工5', '否', NULL, '3111241884', '3111341568', '否', '小黄', '95', '好');
INSERT INTO `delegate` VALUES (8, '186659', '3200186427562564', '演示牧场', '3', '2023-6-25', '2022-9-25', '自然发情', '员工2', '否', NULL, '3112248845', '31241845', '否', '小李', '196', '好');
INSERT INTO `delegate` VALUES (9, '186436', '320000186514785', '演示牧场', '6', '2023-6-25', '2020-10-15', '自然发情', '员工9', '否', NULL, '354868845', '312454845', '否', '小王', '195', '好');

-- ----------------------------
-- Table structure for employeemanagement
-- ----------------------------
DROP TABLE IF EXISTS `employeemanagement`;
CREATE TABLE `employeemanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `duniu0` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `duniu4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yucheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qingnianniu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chengnianniu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employeemanagement
-- ----------------------------
INSERT INTO `employeemanagement` VALUES (1, '100', '50', '80', '90', '150');

-- ----------------------------
-- Table structure for empployeedate
-- ----------------------------
DROP TABLE IF EXISTS `empployeedate`;
CREATE TABLE `empployeedate`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `niudusiwang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yufangcuoshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shuliang` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of empployeedate
-- ----------------------------
INSERT INTO `empployeedate` VALUES (1, '难产', '要充分立即接生，保证母牛和犊牛安全，接生工作人员在临产前一定要做好观察记录表，孕妇分娩征兆时一定要赶入待产室分娩，搞好接生前期准备工作，待产室自然环境保持安静环境卫生，路面要垫好柔软草，难产是要请有这方面工作经验的工作人员协助。', 28);
INSERT INTO `empployeedate` VALUES (2, '医护不合理', '一定要用心医护犊牛，维持牛圈的适合温湿度，以防感柒病虫害，保证犊牛身心健康成长发育。', 32);
INSERT INTO `empployeedate` VALUES (3, '母牛年老体弱', '一些年老体弱的母牛因为体质比较低，生产性能降低，有一些母牛妊娠期间喂养管理方法无法跟上，很容易出现胎死腹中和难产状况，制造的小牛效率低下，存活率也比较低。', 25);
INSERT INTO `empployeedate` VALUES (4, '喂养不合理', '提升新生小牛的喂养，给予充足的营养成分精饲料，保证小牛强健身心健康生长发育。', 30);

-- ----------------------------
-- Table structure for estrouscow
-- ----------------------------
DROP TABLE IF EXISTS `estrouscow`;
CREATE TABLE `estrouscow`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dianzibiaoqian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dangqianhuodong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingkaishi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chixushijian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingjieshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zuijiakaishi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zuijiajieshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of estrouscow
-- ----------------------------
INSERT INTO `estrouscow` VALUES (1, '1-120', '20', '15', '1-26', '5', '1-31', '1-28', '1-31');
INSERT INTO `estrouscow` VALUES (2, '1-121', '21', '21', '1-26', '5', '1-31', '1-28', '1-31');
INSERT INTO `estrouscow` VALUES (3, '1-122', '22', '53', '1-26', '5', '1-31', '1-28', '1-31');
INSERT INTO `estrouscow` VALUES (4, '1-123', '23', '42', '1-26', '5', '1-31', '1-28', '1-31');
INSERT INTO `estrouscow` VALUES (5, '1-124', '24', '35', '1-26', '5', '1-31', '1-28', '1-31');

-- ----------------------------
-- Table structure for estrusrecord
-- ----------------------------
DROP TABLE IF EXISTS `estrusrecord`;
CREATE TABLE `estrusrecord`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zuijin24` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dangqianhuodong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingkashi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingjieshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `faqingshichang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zuijiapeizhong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of estrusrecord
-- ----------------------------
INSERT INTO `estrusrecord` VALUES (1, '1-120', '20', '15', '2020-1-26', '2020-1-31', '5', '2020-1-28');
INSERT INTO `estrusrecord` VALUES (2, '1-121', '25', '15', '2020-1-26', '2020-1-31', '5', '2020-1-28');
INSERT INTO `estrusrecord` VALUES (3, '1-122', '32', '20', '2020-1-26', '2020-1-31', '5', '2020-1-28');
INSERT INTO `estrusrecord` VALUES (4, '1-123', '30', '16', '2020-1-26', '2020-1-31', '5', '2020-1-28');
INSERT INTO `estrusrecord` VALUES (5, '1-124', '11', '16', '2020-1-26', '2020-1-31', '5', '2020-1-28');

-- ----------------------------
-- Table structure for examinationrecords
-- ----------------------------
DROP TABLE IF EXISTS `examinationrecords`;
CREATE TABLE `examinationrecords`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `guanlihao` int(11) NOT NULL,
  `niushe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chandu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taici` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zaozhengchan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sihuotai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taiyi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chanhou` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jiancha` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zigong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `luanchao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jiancharen` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caozuoren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of examinationrecords
-- ----------------------------
INSERT INTO `examinationrecords` VALUES ('1', 201110, '东牛舍', '2022-03-15', '1', '正产', '活胎', '正常', '28', '2022-11-12', '正常', '正常', '小王', '演示用户');
INSERT INTO `examinationrecords` VALUES ('2', 201110, '西牛舍', '2023-04-01', '1', '正产', '活胎', '正常', '28', '2021-5-16', '正常', '正常', '小张', '演示用户');
INSERT INTO `examinationrecords` VALUES ('3', 201110, '北牛舍', '2023-01-30', '1', '正产', '活胎', '正常', '28', '2022-12-12', '正常', '正常', '小张', '演示用户');

-- ----------------------------
-- Table structure for farmprofitanalysis
-- ----------------------------
DROP TABLE IF EXISTS `farmprofitanalysis`;
CREATE TABLE `farmprofitanalysis`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `riqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lirun` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaoduniu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaoyucheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siiaoqingnian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaomiru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaogannai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yaopinduniu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yaopinyucheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yaopinqingnian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yaopinchengmu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jingyeduniu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jingyeyucheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jingyeqingnian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jingyechengmu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshouniunai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshougongdu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshoumudu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshouyucheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshouqingnian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshouchengmu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of farmprofitanalysis
-- ----------------------------
INSERT INTO `farmprofitanalysis` VALUES (1, '2022-01', '156', '465', '4894', '561', '68486', '518', '456', '18', '4651', '984', '8165', '198', '561', '198', '1561', '984', '651', '81', '16518', '9165');
INSERT INTO `farmprofitanalysis` VALUES (2, '2022-02', '165', '18', '6189', '1', '651', '6', '85', '6465', '1', '56189', '4', '651984', '651984', '981654894', '5198489', '156489', '4561894', '1654894', '561489', '481');
INSERT INTO `farmprofitanalysis` VALUES (3, '2022-03', '189', '4', '56198', '456', '184', '84', '561', '84', '654', '89', '4516', '5465', '84', '456', '4894', '89456', '4894', '84', '564', '48');
INSERT INTO `farmprofitanalysis` VALUES (4, '2022-04', '486', '456', '48', '48', '465', '4', '65456', '4', '21', '345', '6489', '1', '561', '684', '4', '561', '6848', '9465', '6', '8456');
INSERT INTO `farmprofitanalysis` VALUES (5, '2022-05', '1564', '565', '4894', '564', '8465', '4', '6489', '456', '165', '489', '456', '48', '456', '48', '489', '456', '489', '4', '654', '65489');

-- ----------------------------
-- Table structure for feedinganalysis
-- ----------------------------
DROP TABLE IF EXISTS `feedinganalysis`;
CREATE TABLE `feedinganalysis`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `xiangmu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chuqishu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `diaobodiaoru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanqunzhuanru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gouru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhihuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `neibugouru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chusheng` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `diaobodiaoru2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanqunzhaunchu` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoji` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `neibuxiaoshou` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshou` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhihuan2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feiliuyang` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dangqisiwang` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of feedinganalysis
-- ----------------------------
INSERT INTO `feedinganalysis` VALUES (1, '生产性生物资产', '235', '15', '16', '2', '9', '3', '6', '4', '7', '15', '5', '12', '6', '3', '4');
INSERT INTO `feedinganalysis` VALUES (2, '成熟生物资产', '157', '13', '3', '6', '4', '10', '16', '7', '15', '13', '7', '6', '9', '10', '13');
INSERT INTO `feedinganalysis` VALUES (3, '泌乳牛', '139', '3', '16', '4', '15', '14', '9', '17', '8', '14', '5', '9', '4', '13', '4');
INSERT INTO `feedinganalysis` VALUES (4, '已孕泌乳牛', '37', '16', '3', '9', '6', '4', '4', '13', '7', '3', '14', '6', '17', '9', '10');
INSERT INTO `feedinganalysis` VALUES (5, '未孕泌乳牛', '95', '10', '13', '7', '16', '9', '10', '20', '16', '9', '7', '16', '18', '6', '7');
INSERT INTO `feedinganalysis` VALUES (6, '产犊在60天内(未配)', '135', '16', '3', '7', '17', '9', '13', '6', '20', '10', '15', '4', '13', '17', '6');
INSERT INTO `feedinganalysis` VALUES (7, '产犊在60天以上(未配)', '25', '19', '4', '8', '17', '14', '14', '7', '11', '16', '18', '9', '17', '13', '5');
INSERT INTO `feedinganalysis` VALUES (8, '已配种但未确认受孕', '75', '9', '16', '14', '9', '7', '3', '10', '9', '4', '7', '9', '11', '13', '17');
INSERT INTO `feedinganalysis` VALUES (9, '已配种但未确认受孕', '364', '4', '9', '19', '23', '75', '9', '7', '16', '3', '16', '19', '13', '19', '17');
INSERT INTO `feedinganalysis` VALUES (10, '干奶牛', '205', '16', '17', '13', '9', '17', '3', '10', '9', '4', '11', '27', '15', '13', '16');
INSERT INTO `feedinganalysis` VALUES (11, '已孕牛', '345', '15', '10', '15', '5', '3', '9', '20', '16', '5', '16', '23', '10', '11', '4');

-- ----------------------------
-- Table structure for goodsmanagement
-- ----------------------------
DROP TABLE IF EXISTS `goodsmanagement`;
CREATE TABLE `goodsmanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `siliangbianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliangmingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaoleibie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `binyinjianma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `siliaofenlei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goodsmanagement
-- ----------------------------
INSERT INTO `goodsmanagement` VALUES (1, '1-101', '青绿饲料', '类别1', 'qlsl', '糠麸类');
INSERT INTO `goodsmanagement` VALUES (2, '1-101', '粗饲料', '类别2', 'csl', '糠麸类');
INSERT INTO `goodsmanagement` VALUES (3, '1-101', '蛋白饲料', '类别3', 'dbsl', '糠麸类');
INSERT INTO `goodsmanagement` VALUES (4, '1-101', '能量饲料', '类别4', 'nlsl', '糠麸类');
INSERT INTO `goodsmanagement` VALUES (5, '1-101', '精饲料', '类别5', 'jsl', '糠麸类');

-- ----------------------------
-- Table structure for healthamalysis
-- ----------------------------
DROP TABLE IF EXISTS `healthamalysis`;
CREATE TABLE `healthamalysis`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `jibingleixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pingjunzhiliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caicifabing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yuedufabing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jibingfabing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of healthamalysis
-- ----------------------------
INSERT INTO `healthamalysis` VALUES (1, '乳房炎', '1-2周', '25', '5%', '3%');
INSERT INTO `healthamalysis` VALUES (2, '产后瘫痪', '1-2周', '15', '3%', '6%');
INSERT INTO `healthamalysis` VALUES (3, '胎衣不下', '2-3周', '10', '2%', '1%');
INSERT INTO `healthamalysis` VALUES (4, '奶牛不孕', '1-3周', '12', '2.4%', '3.2%');
INSERT INTO `healthamalysis` VALUES (5, '酮病', '1-2周', '25', '3.4%', '1.5%');
INSERT INTO `healthamalysis` VALUES (9, '乳头内陷', '1-3周', '3', '2%', '20');
INSERT INTO `healthamalysis` VALUES (10, '假乳', '3-5周', '10', '5%', '15');

-- ----------------------------
-- Table structure for inboundoutbound
-- ----------------------------
DROP TABLE IF EXISTS `inboundoutbound`;
CREATE TABLE `inboundoutbound`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `danjubianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cangkumingc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cangkuleixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gonghuodanwei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rukuriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chuliriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caozuoren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jine` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inboundoutbound
-- ----------------------------
INSERT INTO `inboundoutbound` VALUES (1, '1001', '一号仓库', '饲料仓库', 'XXX供货商', '2021-1-10', '2021-5-12', '张某某', '1200元');
INSERT INTO `inboundoutbound` VALUES (2, '1002', '一号仓库', '饲料仓库', 'XXX供货商', '2021-1-15', '2021-4-12', '张某某', '3052元');
INSERT INTO `inboundoutbound` VALUES (3, '1003', '一号仓库', '饲料仓库', 'XXX供货商', '2021-2-10', '2021-3-12', '张某某', '1250元');
INSERT INTO `inboundoutbound` VALUES (4, '1004', '一号仓库', '饲料仓库', 'XXX供货商', '2021-4-15', '2021-5-17', '张某某', '2500元');
INSERT INTO `inboundoutbound` VALUES (5, '1005', '一号仓库', '饲料仓库', 'XXX供货商', '2021-3-18', '2021-4-14', '张某某', '3070元');
INSERT INTO `inboundoutbound` VALUES (6, '1001', '一号仓库', '药物仓库', 'XXX供货商', '2021-1-10', '2021-5-12', '王某某', '1200元');
INSERT INTO `inboundoutbound` VALUES (7, '1002', '一号仓库', '药物仓库', 'XXX供货商', '2021-1-15', '2021-4-12', '王某某', '3052元');
INSERT INTO `inboundoutbound` VALUES (8, '1003', '一号仓库', '药物仓库', 'XXX供货商', '2021-2-10', '2021-3-12', '王某某', '1250元');
INSERT INTO `inboundoutbound` VALUES (9, '1004', '一号仓库', '药物仓库', 'XXX供货商', '2021-4-15', '2021-5-17', '王某某', '2500元');
INSERT INTO `inboundoutbound` VALUES (10, '1005', '一号仓库', '药物仓库', 'XXX供货商', '2021-3-18', '2021-4-14', '王某某', '3070元');

-- ----------------------------
-- Table structure for index_notes
-- ----------------------------
DROP TABLE IF EXISTS `index_notes`;
CREATE TABLE `index_notes`  (
  `notes_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '备忘id',
  `notes_title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备忘标题',
  `notes_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备忘内容',
  `notes_date` date NULL DEFAULT NULL COMMENT '备忘日期',
  `notes_time` time NULL DEFAULT NULL COMMENT '备忘时间',
  `notes_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`notes_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of index_notes
-- ----------------------------
INSERT INTO `index_notes` VALUES (2, '321', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-06', '19:07:25', '3');
INSERT INTO `index_notes` VALUES (4, '444', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '3');
INSERT INTO `index_notes` VALUES (5, '555', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '3');
INSERT INTO `index_notes` VALUES (6, '6666', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '3');
INSERT INTO `index_notes` VALUES (7, '6666', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '3');
INSERT INTO `index_notes` VALUES (8, '6666', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '3');
INSERT INTO `index_notes` VALUES (9, '6666', '啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打啊啊啊啊哇大苏打', '2023-05-05', '10:07:48', '1');
INSERT INTO `index_notes` VALUES (10, '999', '1231231231231231', '2023-05-05', '10:07:48', '1');
INSERT INTO `index_notes` VALUES (13, '哈哈哈哈哈哈哈哈哈哈呵呵', '是哈哈哈哈', '2023-05-06', '21:05:21', '3');
INSERT INTO `index_notes` VALUES (14, '哈哈哈哈哈哈哈哈哈哈呵呵', '是哈哈哈哈', '2023-05-06', '21:05:21', '3');
INSERT INTO `index_notes` VALUES (15, '就哈哈哈哈', '哈哈哈哈hh哈哈哈哈哈呵呵', '2023-05-06', '21:07:35', '3');
INSERT INTO `index_notes` VALUES (16, '坤坤kk', '你干嘛 哎呦', '2023-05-06', '21:14:22', '3');
INSERT INTO `index_notes` VALUES (17, '反反复复烦烦烦', '他吞吞吐吐', '2023-05-06', '21:14:55', '3');
INSERT INTO `index_notes` VALUES (18, '强强强强', '啊啊啊啊', '2023-05-06', '21:15:53', '3');
INSERT INTO `index_notes` VALUES (19, '咿呀咿呀哟', '三生三世十里桃花', '2023-05-06', '21:16:49', '3');
INSERT INTO `index_notes` VALUES (20, '坤坤坤', '你干嘛 哎呦 黑恶hi', '2023-05-06', '21:14:22', '3');
INSERT INTO `index_notes` VALUES (21, '阿斯顿撒', '啊实打', '2023-05-06', '22:10:31', '3');
INSERT INTO `index_notes` VALUES (22, '吃饭', '吃桃李还是什么东西', '2023-05-07', '18:09:06', '1');

-- ----------------------------
-- Table structure for individualmilkproduction
-- ----------------------------
DROP TABLE IF EXISTS `individualmilkproduction`;
CREATE TABLE `individualmilkproduction`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niushebianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niusheleibie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taici` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `miruzhuangtai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `channairiqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yicenailiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ercenailiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dangtiannailiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chanduriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nirutianshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zaitaitianshu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of individualmilkproduction
-- ----------------------------
INSERT INTO `individualmilkproduction` VALUES (1, '1-1201', '2-101', '大牛舍', '1', '正常', '2022-2-5', '10L/次', '8L/次', '50', '2023-1-30', '30', '280');
INSERT INTO `individualmilkproduction` VALUES (2, '1-1202', '2-102', '大牛舍', '2', '正常', '2022-2-6', '10L/次', '8L/次', '60', '2023-1-31', '29', '270');
INSERT INTO `individualmilkproduction` VALUES (3, '1-1203', '2-103', '大牛舍', '3', '正常', '2022-2-7', '11L/次', '8L/次', '30', '2023-2-1', '24', '275');
INSERT INTO `individualmilkproduction` VALUES (4, '1-1204', '2-103', '小牛舍', '2', '正常', '2022-2-10', '10L/次', '8L/次', '35', '2023-2-12', '31', '285');
INSERT INTO `individualmilkproduction` VALUES (5, '1-1205', '2-104', '小牛舍', '2', '正常', '2022-2-10', '10L/次', '8L/次', '25', '2023-2-12', '31', '285');
INSERT INTO `individualmilkproduction` VALUES (6, '1-1206', '2-104', '小牛舍', '2', '正常', '2022-2-10', '10L/次', '8L/次', '35', '2023-2-12', '31', '285');

-- ----------------------------
-- Table structure for kilogramofmilk
-- ----------------------------
DROP TABLE IF EXISTS `kilogramofmilk`;
CREATE TABLE `kilogramofmilk`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `leixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `may` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jun` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jul` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `aug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sep` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oct` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nov` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `decs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kilogramofmilk
-- ----------------------------
INSERT INTO `kilogramofmilk` VALUES (1, '饲料', '0', '50', '0', '50', '20', '80', '0', '0', '40', '20', '0', '0');
INSERT INTO `kilogramofmilk` VALUES (2, '药品', '0', '50', '0', '20', '0', '70', '60', '80', '0', '40', '0', '70');
INSERT INTO `kilogramofmilk` VALUES (3, '冻精', '0', '40', '0', '80', '0', '60', '0', '50', '70', '0', '80', '0');
INSERT INTO `kilogramofmilk` VALUES (4, '人工', '0', '80', '0', '60', '0', '80', '60', '60', '50', '0', '60', '60');
INSERT INTO `kilogramofmilk` VALUES (5, '其他', '0', '70', '0', '60', '50', '50', '0', '70', '80', '0', '10', '20');
INSERT INTO `kilogramofmilk` VALUES (6, '总计', '0', '70', '0', '60', '0', '70', '0', '50', '80', '0', '80', '50');

-- ----------------------------
-- Table structure for marrecords
-- ----------------------------
DROP TABLE IF EXISTS `marrecords`;
CREATE TABLE `marrecords`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dengjiriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taici` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yongliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yongyaofangshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yongyaorenyuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caozuoren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caozuoriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of marrecords
-- ----------------------------
INSERT INTO `marrecords` VALUES (1, '186001', '2022-09-22', '2', '5.0', 'pg1', '员工2', '员工2', '2023-01-22');
INSERT INTO `marrecords` VALUES (2, '186015', '2022-07-02', '2', '5.0', 'pg1', '员工1', '员工1', '2022-11-26');
INSERT INTO `marrecords` VALUES (3, '186097', '2023-02-16', '2', '5.0', 'pg1', '员工4', '员工4', '2023-07-16');
INSERT INTO `marrecords` VALUES (4, '186546', '2020-12-23', '2', '5.0', 'pg1', '员工8', '员工8', '2022-07-16');
INSERT INTO `marrecords` VALUES (5, '181545', '2022-09-17', '2', '5.0', 'pg1', '员工5', '员工5', '2023-06-02');
INSERT INTO `marrecords` VALUES (6, '186545', '2020-09-22', '2', '5.0', 'pg1', '员工16', '员工16', '2022-07-12');
INSERT INTO `marrecords` VALUES (7, '186557', '2021-01-20', '2', '5.0', 'pg1', '员工6', '员工6', '2021-07-06');

-- ----------------------------
-- Table structure for materialmanagement
-- ----------------------------
DROP TABLE IF EXISTS `materialmanagement`;
CREATE TABLE `materialmanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `cangkumingc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cangkuleix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cangkuzhuangtai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of materialmanagement
-- ----------------------------
INSERT INTO `materialmanagement` VALUES (1, '草料库房', '饲料', '冻结');
INSERT INTO `materialmanagement` VALUES (2, '冻精库房', '精液', '冻结');
INSERT INTO `materialmanagement` VALUES (3, '药品库1', '药品', '冻结');
INSERT INTO `materialmanagement` VALUES (4, '药品库2', '药品', '冻结');
INSERT INTO `materialmanagement` VALUES (5, '药品库3', '药品', '冻结');
INSERT INTO `materialmanagement` VALUES (6, '低耗库房1', '耗材', '冻结');

-- ----------------------------
-- Table structure for menu_manage
-- ----------------------------
DROP TABLE IF EXISTS `menu_manage`;
CREATE TABLE `menu_manage`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `menu_icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `menu_index` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由路径',
  `menu_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '完整路径',
  `menu_level` bigint(20) NULL DEFAULT NULL COMMENT '级别（0：顶级；id：二级）',
  `menu_sort` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `menu_show` bigint(20) NULL DEFAULT NULL COMMENT '显示状态（0隐藏；1显示）',
  `menu_rights` int(11) NULL DEFAULT NULL COMMENT '权限（0：管理员；1：销售员）',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menu_manage
-- ----------------------------
INSERT INTO `menu_manage` VALUES (1, '大数据分析', 'el-icon-s-home', '', '', 0, 1, 1, 0);
INSERT INTO `menu_manage` VALUES (2, '繁殖分析', 'el-icon-user', '/employeeData', '/dataSreening/employeeData', 1, NULL, 1, 1);
INSERT INTO `menu_manage` VALUES (3, '健康分析', 'el-icon-data-line', '/healthAnalysis', '/purchase/healthAnalysis', 11, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (4, '系统设置', 'el-icon-s-tools', '', '', 0, 8, 1, 1);
INSERT INTO `menu_manage` VALUES (5, '菜单设置', 'el-icon-document', '/menuManage', '/system/menuManage.vue', 4, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (7, '用户管理', 'el-icon-user', '/userManagement', '/system/userManagement.vue', 4, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (8, '奶厅数据', 'el-icon-s-marketing', '', '', 0, 2, 1, 1);
INSERT INTO `menu_manage` VALUES (9, '理化指标记录', 'el-icon-truck', '/salesRecord', '/sales/salesRecord.vue', 8, NULL, 1, 1);
INSERT INTO `menu_manage` VALUES (10, '其他奶量记录', 'el-icon-sold-out', '/scarceRecord', '/sales/scarceRecord.vue', 8, NULL, 1, 1);
INSERT INTO `menu_manage` VALUES (11, '保健管理', 'el-icon-s-goods', '', '', 0, 3, 1, 0);
INSERT INTO `menu_manage` VALUES (12, '健康登记', 'el-icon-tickets', '/orderManagement', '/purchase/orderManagement.vue', 11, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (13, '健康日志', 'el-icon-truck', '/transportationManagement', '/purchase/transportationManagement.vue', 11, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (14, '饲养管理', 'el-icon-s-cooperation', '', '', 0, 4, 1, 1);
INSERT INTO `menu_manage` VALUES (15, '牛舍管理', 'el-icon-document', '/categoryManagement', '/stock/categoryManagement.vue', 14, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (16, '奶牛管理', 'el-icon-school', '/unitManagement', '/stock/unitManagement.vue', 14, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (17, '配方管理', 'el-icon-house', '/storehouseManagement', '/stock/storehouseManagement.vue', 14, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (18, '饲料管理', 'el-icon-shopping-bag-1', '/goodsManagement', '/stock/goodsManagement.vue', 14, NULL, 1, 1);
INSERT INTO `menu_manage` VALUES (19, '繁育管理', 'el-icon-menu', '', NULL, 0, 5, 1, 0);
INSERT INTO `menu_manage` VALUES (20, '待办事项', 'el-icon-setting', '/storeManagement', '/store/storeManagement.vue', 19, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (21, '繁育记录', 'el-icon-edit-outline', '/delegate', '/store/delegate.vue', 19, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (22, '财务管理', 'el-icon-s-finance', '', '', 0, 7, 1, 0);
INSERT INTO `menu_manage` VALUES (23, '分摊比例参数设置', 'el-icon-user', '/employeeManagement', '/employee/employeeManagement.vue', 22, NULL, 1, 0);
INSERT INTO `menu_manage` VALUES (24, '个体产奶记录', 'el-icon-pie-chart', '/individualMilkProduction', '/sales/individualMilkProduction', 8, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (25, '计步器', 'el-icon-s-promotion el-icon-s-home', '', '', 0, 6, 1, 0);
INSERT INTO `menu_manage` VALUES (26, '未入罐奶量记录', 'el-icon-takeaway-box', '/untappedMilk', '/sales/untappedMilk', 8, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (27, '兽医分析', 'el-icon-magic-stick', '/salesData', '/dataSreening/salesData', 1, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (28, '物资管理', 'el-icon-s-grid', '', '', 0, 9, 1, 0);
INSERT INTO `menu_manage` VALUES (29, '班次管理', 'el-icon-date', '/shiftManagement', '/stock/shiftManagement', 14, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (30, '子宫检查记录', 'el-icon-edit-outline', '/examinationrecords', '/store/examinationrecords.vue', 19, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (31, '围产记录', 'el-icon-tickets', '/Perinatalrecords', '/store/Perinatalrecords.vue', 19, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (32, '用药记录', 'el-icon-document', '/MARrecords', '/store/MARrecords.vue', 19, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (33, '发情牛', 'el-icon-view', '/estrousCow', '/pedometer/estrousCow', 25, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (34, '异常数据管理', 'el-icon-takeaway-box', '/abnormalData', '/pedometer/abnormalData', 25, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (35, '牛只发情记录', 'el-icon-document', '/estrusRecord', '/pedometer/estrusRecord', 25, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (36, '牛只资产卡', 'el-icon-takeaway-box', '/bullAsset', '/employee/bullAsset', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (37, '月度费用输入', 'el-icon-data-line', '/monthlyFeeInput', '/employee/monthlyFeeInput', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (38, '现金成本占比', 'el-icon-data-analysis', '/cashCost', '/employee/cashCost', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (39, '牛场收入分析', 'el-icon-pie-chart', '/cattleFarmIncome', '/employee/cattleFarmIncome', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (40, '牛场支出分析', 'el-icon-data-line', '/CattleFarmExpenditure', '/employee/CattleFarmExpenditure.vue', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (41, '牛场利润分析', 'el-icon-data-board', '/FarmProfitAnalysis', '/employee/FarmProfitAnalysis.vue', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (42, '公斤奶成本', 'el-icon-notebook-1', '/kilogramofmilk', '/employee/kilogramofmilk.vue', 22, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (43, '基本设置', 'el-icon-setting', '/basicSettings', '/material/basicSettings', 28, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (44, '出入库管理', 'el-icon-document-checked', '/inboundOutbound', '/material/inboundOutbound', 28, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (45, '仓库盘点', 'el-icon-document', '/materialmanagement', '/material/materialmanagement', 28, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (46, '数据统计', 'el-icon-edit-outline', '/datastatistics', '/material/datastatistics.vue', 28, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (47, '饲养分析', 'el-icon-data-board', '/feedingAnalysis', '/dataSreening/feedingAnalysis', 1, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (48, '奶厅分析', 'el-icon-house', '/dairyAnalysis', '/dataSreening/dairyAnalysis', 1, 0, 1, 0);
INSERT INTO `menu_manage` VALUES (49, '决策支持', 'el-icon-coordinate', '/decisionSupport', '/dataSreening/decisionSupport', 1, 0, 1, 1);

-- ----------------------------
-- Table structure for monthlyfeeinput
-- ----------------------------
DROP TABLE IF EXISTS `monthlyfeeinput`;
CREATE TABLE `monthlyfeeinput`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `dengjiyuefen` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `baoxianfei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shuidianfei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zuyongshebei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nongjishebei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weixieufei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jinaiting` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of monthlyfeeinput
-- ----------------------------
INSERT INTO `monthlyfeeinput` VALUES (1, '2022-3', '300元', '130元', '1100元', '1310元', '370元', '520元');
INSERT INTO `monthlyfeeinput` VALUES (2, '2022-4', '200元', '110元', '1020元', '1187元', '142元', '416元');
INSERT INTO `monthlyfeeinput` VALUES (3, '2022-5', '350元', '210元', '1300元', '1180元', '150元', '400元');
INSERT INTO `monthlyfeeinput` VALUES (4, '2022-6', '280元', '150元', '1300元', '1380元', '300元', '500元');

-- ----------------------------
-- Table structure for ordermanagement
-- ----------------------------
DROP TABLE IF EXISTS `ordermanagement`;
CREATE TABLE `ordermanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `jibingzhonglei` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chanhouhuli` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tongbing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mangru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qujiao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jianyi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quchong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ordermanagement
-- ----------------------------
INSERT INTO `ordermanagement` VALUES (1, '乳房炎', '20', '15', '25', '32', '12', '18');
INSERT INTO `ordermanagement` VALUES (2, '产后瘫痪', '25', '15', '12', '10', '8', '13');
INSERT INTO `ordermanagement` VALUES (3, '胎衣不下', '32', '20', '12', '16', '18', '20');
INSERT INTO `ordermanagement` VALUES (4, '奶牛不孕', '30', '16', '20', '10', '5', '8');

-- ----------------------------
-- Table structure for perinatalrecords
-- ----------------------------
DROP TABLE IF EXISTS `perinatalrecords`;
CREATE TABLE `perinatalrecords`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niushe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `denglushi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taici` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuansheriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanchushe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanchusheleixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanrushe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuanrusheleixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhixingren` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caizuoshijian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of perinatalrecords
-- ----------------------------
INSERT INTO `perinatalrecords` VALUES (1, '140226', '3', '205', '1', '2023-6-10', '北舍', '泌乳', '18东', '围产', '员工9', '2022-09-10 16:13:01');
INSERT INTO `perinatalrecords` VALUES (2, '140295', '3', '205', '1', '2023-6-16', '东舍', '泌乳', '20西', '围产', '员工7', '2023-03-10 14:25:01');
INSERT INTO `perinatalrecords` VALUES (3, '1401355', '3', '205', '1', '2023-5-10', '东舍', '泌乳', '16北', '围产', '员工4', '2022-09-30 15:13:01');
INSERT INTO `perinatalrecords` VALUES (4, '14021', '3', '205', '1', '2023-4-10', '南舍', '泌乳', '15西', '围产', '员工1', '2022-07-19 16:13:01');
INSERT INTO `perinatalrecords` VALUES (5, '140546', '3', '205', '1', '2023-4-10', '西舍', '泌乳', '15东', '围产', '员工4', '2020-09-10 08:13:01');
INSERT INTO `perinatalrecords` VALUES (6, '140326', '3', '205', '1', '2023-3-18', '东舍', '泌乳', '5南', '围产', '员工6', '2021-01-18 11:13:01');
INSERT INTO `perinatalrecords` VALUES (7, '140219', '3', '205', '1', '2023-3-06', '北舍', '泌乳', '16东', '围产', '员工8', '2020-10-09 20:16:01');
INSERT INTO `perinatalrecords` VALUES (8, '140548', '3', '205', '1', '2023-2-23', '西舍', '泌乳', '18西', '围产', '员工2', '2020-09-10 16:13:01');
INSERT INTO `perinatalrecords` VALUES (9, '140264', '3', '205', '1', '2023-1-06', '东舍', '泌乳', '34南', '围产', '员工4', '2022-04-10 23:13:01');
INSERT INTO `perinatalrecords` VALUES (10, '140264', '3', '205', '1', '2023-7-06', '东舍', '泌乳', '34南', '围产', '员工4', '2022-04-10 23:13:01');

-- ----------------------------
-- Table structure for purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order`  (
  `order_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `order_corporation` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂家',
  `order_category` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品类别',
  `order_goods` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购商品名',
  `order_num` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购数量',
  `order_unit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `order_price` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '批发单价',
  `order_state` int(11) NULL DEFAULT NULL COMMENT '0（未开始）1（开始）2（完成）',
  `order_date` datetime NULL DEFAULT NULL COMMENT '订单日期',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of purchase_order
-- ----------------------------
INSERT INTO `purchase_order` VALUES (00000035, 'BH20230523', 'XXX厂家', '铸铁弯头,PVC三通', '32铸铁弯头,32PVC三通', '10,10', '个,个', '5,5', 2, '2023-04-26 07:47:03');
INSERT INTO `purchase_order` VALUES (00000036, 'BH20230504', 'XXX厂家', '铸铁管箍,PVC三通', '管箍,三通', '10,10', '个,个', '5,5', 1, '2023-05-04 03:40:17');
INSERT INTO `purchase_order` VALUES (00000037, 'BH20230625', 'XXX水管厂', 'PVC热水管,PVC冷水管', '20热水管,20冷水管', '5,5', '包,包', '200,200', 0, '2023-05-04 04:13:13');

-- ----------------------------
-- Table structure for purchase_transport
-- ----------------------------
DROP TABLE IF EXISTS `purchase_transport`;
CREATE TABLE `purchase_transport`  (
  `transport_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '运输id',
  `transport_mode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运输方式',
  `transport_driver` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '司机名字',
  `transport_drivertel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '司机电话',
  `transport_carnumber` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌号',
  `transport_startdate` date NULL DEFAULT NULL COMMENT '开始日期',
  `transport_enddate` date NULL DEFAULT NULL COMMENT '结束日期',
  `transport_storagehouse` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库房（id）',
  `transport_ordernumber` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单id',
  `transport_state` int(11) NULL DEFAULT NULL COMMENT '0（未开始）1（完成）',
  PRIMARY KEY (`transport_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of purchase_transport
-- ----------------------------
INSERT INTO `purchase_transport` VALUES (00000039, '公路运输', '张三', '15555555555', '蒙L6931', '2023-04-26', '2023-04-28', '2', 'BH20230523', 1);
INSERT INTO `purchase_transport` VALUES (00000040, '公路运输', '李四', '15555555555', '蒙L6623', '2023-05-04', '2023-05-10', '4', 'BH20230504', 0);

-- ----------------------------
-- Table structure for sales_record
-- ----------------------------
DROP TABLE IF EXISTS `sales_record`;
CREATE TABLE `sales_record`  (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_order` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `record_goods` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `record_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名',
  `record_num` bigint(20) NULL DEFAULT NULL COMMENT '售出数量',
  `record_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `record_salesperson` bigint(20) NULL DEFAULT NULL COMMENT '销售人员id',
  `record_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单日期',
  PRIMARY KEY (`record_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sales_record
-- ----------------------------
INSERT INTO `sales_record` VALUES (1, 'XXX牛场', 0, '25管箍', 20, 40.00, 3, '2023-01-01 21:24:35');

-- ----------------------------
-- Table structure for salesdate
-- ----------------------------
DROP TABLE IF EXISTS `salesdate`;
CREATE TABLE `salesdate`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `changjianjibing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dabingyuanyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `yufangcuoshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pingjunzhiliaozhouqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of salesdate
-- ----------------------------
INSERT INTO `salesdate` VALUES (1, '乳房水肿', '钠离子总摄取量过高、低蛋白血症等。同时，有观点认为，乳房水肿多发生在分娩前后。肥胖、乳房下垂或产量较高的奶牛发病率较高。该病病因主要是怀孕奶牛干奶期钾离子干奶期和泌乳早期饲喂超量的谷物，可以促使发生本病。临近分娩的母牛乳房轻度水肿属于正常生理现象，产后10天左右即可消失。病理性水肿可能会持续数月，严重的乳房水肿会影响奶牛健康和牛奶品质。乳房水肿可能会导致机器挤奶后，乳头乳池仍存留大量的奶，增加乳房炎的发病率。同时，乳房过度膨胀会导致奶牛起卧、行走困难。', '一周左右', '干奶后期禁喂食盐。同时，控制奶牛干奶期营养状况，调整粗料和精料比例，使母牛在产前达到3.5分膘情，防止奶牛过瘦或过肥。');
INSERT INTO `salesdate` VALUES (2, '乳头末端损伤', '乳头末端损伤最易发生在乳房下垂的牛身上，可能是牛自己同侧后肢的趾或悬蹄造成，或是由于临近牛踢踩乳头造成的，也有舔咬自己乳头造成的。挤奶机压力增加和过度挤奶也会导致发病。挤奶员护理不当可加重病情。由于气候干燥，北方冬季该病发病率较高。患病牛乳头软组织肿胀，有痛感，严重时乳头弥漫性肿胀、出血。母牛反感或拒绝挤奶，导致挤奶不完全，极易诱发乳房炎。', '一到三周左右', '为奶牛提供宽敞的休息.环境，避免过于拥挤。在冬季运动场铺垫秸杆，注意保暖。同时，应注意保持合适的挤奶机压力。一旦发生该病，挤奶员应注意加强护理，机器挤奶后进行人工挤奶。');
INSERT INTO `salesdate` VALUES (3, '难产', '难产是指由于多种原因造成的，如果不进行人工助产，母体不能或难于排出胎儿的产科疾病。难产属危急病症，必须及时的正确助产，否则可危急母子生命。难产可继发产道损伤、产后感染、胎衣不下、子宫脱出、子宫破裂、子宫炎、膀胱破裂、产后截瘫等疾病。难产的直接原因主要有胎儿性难产(胎儿过大、胎位或胎势异常﹑双胎、畸形胎等原因造成的难产)、产道性难产(子宫颈开张不全、阴道及阴门狭窄、子宫扭转等引起的难产)、产力性难产（怀孕奶牛产前激素分泌失调、体弱、患病等原因造成分娩时收缩、努责无力)。', '两周左右', '尽量保持胎儿出生重是初产胎儿40公斤左右，经产胎儿45公斤左右，初产牛适龄配种在15月龄以上，控制怀孕后期精饲料喂量，避免因胎儿相对和绝对过大发生难产。同时，不要过早的干扰母牛的正常生产，给母牛创造一个安静、卫生、宽敞的分娩环境，尽量让母牛自然分娩。');
INSERT INTO `salesdate` VALUES (4, '产道裂伤和产后感染', '产道裂伤主要是因为胎儿过大或助产时间过早所致。产道裂伤容易引起阴道炎，阴门炎，子宫炎，败血症等产后感染。轻者影响母牛的健康，产奶减少，发情、配种延迟，严重的感染可引发急性死亡，或转为慢性炎症。病牛逐渐消瘦，久配不孕而被淘汰。', '三周左右', '初配母牛应尽量选择体形较小的公牛精液;产犊时，尽量让母牛自然分娩，不要急于助产﹔保持良好的卫生条件，严格消毒，助产时，可向产道涂布石蜡油或其他润滑剂;一旦发生产道裂伤，可在患处涂布抗生素，如果发生严重裂伤时，应进行缝合、止血、注射抗生素全身治疗，控制感染，加速伤口愈合。');
INSERT INTO `salesdate` VALUES (5, '产后瘫痪（生产瘫痪)', '产后瘫痪多发生于产后0一3天营养较好、奶产量较高的经产牛，产后12一48小时内多发。病牛表现特征是低血钙、卧地不起、四肢瘫痪、呈睡眠状态，反应迟钝，触摸皮肤发凉，脉搏快而弱，掀起尾巴无抗力，严重的知觉丧失。病情发展很快，如不及时治疗，50%以上的病牛可在12一48内死亡。', '三周左右', '产后瘫痪多发生于产后0一3天营养较好、奶产量较高的经产牛，产后12一48小时内多发。病牛表现特征是低血钙、卧地不起、四肢瘫痪、呈睡眠状态，反应迟钝，触摸皮肤发凉，脉搏快而弱，掀起尾巴无抗力，严重的知觉丧失。病情发展很快，如不及时治疗，50%以上的病牛可在12一48内死亡。');
INSERT INTO `salesdate` VALUES (6, '胎衣不下', '在奶牛，胎衣不下或胎盘滞留是一种产后常见疾病。胎衣应在正常分娩后8h 内排出。因此，胎衣超过8-12h不下就被认为是不正常的。正常健康奶牛分娩后胎衣不下的发生率在3%-12%之间，平均为7%，而异常分娩的（如剖腹产、难产、流产、早产）母牛和感染布鲁氏杆菌病的牛群胎衣不下的发生率在20%-50%，甚至更高。胎衣不下不但引起产奶量下降，还可以引起子宫内膜炎和子宫复旧延迟，从而导致不孕，致使许多奶牛被迫提前淘汰。', '三周左右', '加强饲养管理，提供富含维生素、矿物质的全价日粮，增加干奶期奶牛的活动量，保证奶牛膘情适中﹔尽量让母牛自然分娩，助产时注意卫和生消毒，严格按操作规程执行;产后让母牛舔食胎儿身上的羊水或马上给母牛注射傕产素、饮服温热红糖麸皮水等，促进胎衣尽快排出。');

-- ----------------------------
-- Table structure for salesrecordother
-- ----------------------------
DROP TABLE IF EXISTS `salesrecordother`;
CREATE TABLE `salesrecordother`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `niuchangmingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chepai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jinchangshunxu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `riqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `naiguanhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `caozuoriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of salesrecordother
-- ----------------------------
INSERT INTO `salesrecordother` VALUES (1, 'XXX牛场1', '蒙A252A8', '1', '2022-12-30', '1-2503', '2023-2-5');
INSERT INTO `salesrecordother` VALUES (2, 'XXX牛场2', '蒙A862A8', '1', '2022-12-30', '1-2504', '2023-2-5');
INSERT INTO `salesrecordother` VALUES (3, 'XXX牛场3', '蒙A24128', '1', '2022-12-30', '1-2505', '2023-2-5');
INSERT INTO `salesrecordother` VALUES (4, 'XXX牛场4', '蒙A785A8', '1', '2022-12-30', '1-2506', '2023-2-5');
INSERT INTO `salesrecordother` VALUES (5, 'XXX牛场5', '蒙A2548', '1', '2022-12-30', '1-2507', '2023-2-5');

-- ----------------------------
-- Table structure for scarcerecord
-- ----------------------------
DROP TABLE IF EXISTS `scarcerecord`;
CREATE TABLE `scarcerecord`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `riqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuzhishuliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zongnailiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pingjunchannai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `beizhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of scarcerecord
-- ----------------------------
INSERT INTO `scarcerecord` VALUES (5, '2023-1-3', '1000', '10000', '10', '无');
INSERT INTO `scarcerecord` VALUES (4, '2023-2-4', '1000', '11000', '11', '无');
INSERT INTO `scarcerecord` VALUES (3, '2023-3-5', '1000', '12000', '12', '无');
INSERT INTO `scarcerecord` VALUES (2, '2023-4-6', '1000', '13000', '13', '无');
INSERT INTO `scarcerecord` VALUES (1, '2023-5-7', '1000', '14000', '14', '无');
INSERT INTO `scarcerecord` VALUES (6, '2023-6-4', '1000', '15200', '15', '无');
INSERT INTO `scarcerecord` VALUES (45, '2023-6-5', '500', '5000', '5', '无');

-- ----------------------------
-- Table structure for shiftmanagement
-- ----------------------------
DROP TABLE IF EXISTS `shiftmanagement`;
CREATE TABLE `shiftmanagement`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `banci` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fenzhong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shiftmanagement
-- ----------------------------
INSERT INTO `shiftmanagement` VALUES (1, '班次1', '10', '50');
INSERT INTO `shiftmanagement` VALUES (2, '班次2', '8', '30');
INSERT INTO `shiftmanagement` VALUES (3, '班次3', '9', '35');
INSERT INTO `shiftmanagement` VALUES (4, '班次4', '14', '25');
INSERT INTO `shiftmanagement` VALUES (5, '班次5', '12', '30');

-- ----------------------------
-- Table structure for stock_category
-- ----------------------------
DROP TABLE IF EXISTS `stock_category`;
CREATE TABLE `stock_category`  (
  `category_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '类别id',
  `category_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类名',
  `category_notes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_category
-- ----------------------------
INSERT INTO `stock_category` VALUES (00000003, 'PVC弯头', 'PVC管件');
INSERT INTO `stock_category` VALUES (00000004, '铸铁管箍', '铸铁管件');
INSERT INTO `stock_category` VALUES (00000005, '铸铁暖气片', '低端暖气片');
INSERT INTO `stock_category` VALUES (00000006, '锅炉', '大型锅炉');
INSERT INTO `stock_category` VALUES (00000048, 'PVC三通', 'PVC管件');
INSERT INTO `stock_category` VALUES (00000049, '铸铁弯头', '铸铁管件');
INSERT INTO `stock_category` VALUES (00000050, 'PVC管箍', 'PVC管件');
INSERT INTO `stock_category` VALUES (00000051, '家用锅炉', '家用小型锅炉');
INSERT INTO `stock_category` VALUES (00000052, 'PVC热水管', '小型管件');
INSERT INTO `stock_category` VALUES (00000053, 'PVC冷水管', '小型管件');
INSERT INTO `stock_category` VALUES (00000054, '塑料排水管道', '大型管道');
INSERT INTO `stock_category` VALUES (00000055, '塑料水龙头', '低端小型水龙头');
INSERT INTO `stock_category` VALUES (00000056, '铜水龙头', '全铜高端水龙头');

-- ----------------------------
-- Table structure for stock_goods
-- ----------------------------
DROP TABLE IF EXISTS `stock_goods`;
CREATE TABLE `stock_goods`  (
  `goods_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_category` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `goods_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名',
  `goods_price` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '价格',
  `goods_quantity` bigint(20) NULL DEFAULT NULL COMMENT '数量',
  `goods_unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `goods_storehouse` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储库房',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_goods
-- ----------------------------
INSERT INTO `stock_goods` VALUES (00000025, '暖气片', '4组陶瓷暖气片', '100', 50, '个', '3');
INSERT INTO `stock_goods` VALUES (00000028, 'PVC弯头', '25弯头', '10', 100, '个', '4');
INSERT INTO `stock_goods` VALUES (00000029, '铸铁管箍', '32管箍', '5', 40, '个', '4');
INSERT INTO `stock_goods` VALUES (00000030, '铸铁暖气片', '5组暖气片', '100', 5, '组', '2');
INSERT INTO `stock_goods` VALUES (00000031, '锅炉', 'XX牌大型锅炉', '2000', 2, '台', '2');
INSERT INTO `stock_goods` VALUES (00000032, 'PVC三通', '32三通', '10', 200, '个', '2');
INSERT INTO `stock_goods` VALUES (00000033, '铸铁弯头', '40弯头', '10', 130, '个', '1');
INSERT INTO `stock_goods` VALUES (00000034, '塑料排水管道', '100排水管', '100', 1, '根', '4');
INSERT INTO `stock_goods` VALUES (00000035, '铜水龙头', 'XXX牌水龙头', '30', 6, '个', '4');
INSERT INTO `stock_goods` VALUES (00000036, '家用锅炉', 'XXX牌小型锅炉', '1000', 14, '台', '4');
INSERT INTO `stock_goods` VALUES (00000037, 'PVC三通', '42三通', '5', 197, '个', '4');
INSERT INTO `stock_goods` VALUES (00000038, 'PVC三通', '32三通', '5', 100, '个', '4');
INSERT INTO `stock_goods` VALUES (00000039, '铸铁弯头', '32弯头', '10', 80, '个', '4');
INSERT INTO `stock_goods` VALUES (00000040, '铸铁管箍', '32铸铁管箍', '10', 45, '个', '4');
INSERT INTO `stock_goods` VALUES (00000041, '铸铁弯头', '32铸铁弯头', '5', 10, '个', '2');
INSERT INTO `stock_goods` VALUES (00000042, 'PVC三通', '32PVC三通', '5', 10, '个', '2');
INSERT INTO `stock_goods` VALUES (00000043, '铸铁弯头', '32铸铁弯头', '5', 10, '个', '2');
INSERT INTO `stock_goods` VALUES (00000044, 'PVC三通', '32PVC三通', '5', 10, '个', '2');

-- ----------------------------
-- Table structure for stock_storehouse
-- ----------------------------
DROP TABLE IF EXISTS `stock_storehouse`;
CREATE TABLE `stock_storehouse`  (
  `storehouse_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '库房id',
  `storehouse_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库房名',
  `storehouse_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `storehouse_notes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`storehouse_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_storehouse
-- ----------------------------
INSERT INTO `stock_storehouse` VALUES (00000001, '精液信息', NULL, NULL);
INSERT INTO `stock_storehouse` VALUES (00000002, '购销单位管理', NULL, NULL);
INSERT INTO `stock_storehouse` VALUES (00000003, '耗材信息', NULL, NULL);
INSERT INTO `stock_storehouse` VALUES (00000004, '药品信息', NULL, NULL);
INSERT INTO `stock_storehouse` VALUES (00000005, '饲料信息', '临河', '五金电料');
INSERT INTO `stock_storehouse` VALUES (00000006, '仓库信息', '口岸', '卫浴厨具');
INSERT INTO `stock_storehouse` VALUES (00000007, '物资周转日设定', '物流园', '管件暖器配件');

-- ----------------------------
-- Table structure for stock_unit
-- ----------------------------
DROP TABLE IF EXISTS `stock_unit`;
CREATE TABLE `stock_unit`  (
  `unit_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '单位id',
  `unit_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位名',
  `unit_notes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位备注',
  PRIMARY KEY (`unit_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_unit
-- ----------------------------
INSERT INTO `stock_unit` VALUES (00000001, '根', '管道，线槽等');
INSERT INTO `stock_unit` VALUES (00000002, '包', '各种管件，保温材料等');
INSERT INTO `stock_unit` VALUES (00000003, '个', '小型管件，弯头水龙头等');
INSERT INTO `stock_unit` VALUES (00000004, '箱', '箱装配件等');
INSERT INTO `stock_unit` VALUES (00000005, '盒', '螺丝钉子，小型配件等');
INSERT INTO `stock_unit` VALUES (00000007, '组', '铸铁，陶瓷暖气片等');
INSERT INTO `stock_unit` VALUES (00000008, '台', '锅炉，热水器等大型件');
INSERT INTO `stock_unit` VALUES (00000009, '箱', '中型散件，管件等');
INSERT INTO `stock_unit` VALUES (00000010, '袋', '小型散件，管件等');
INSERT INTO `stock_unit` VALUES (00000011, '桶', '润滑油，大桶油漆等');
INSERT INTO `stock_unit` VALUES (00000012, '罐', '喷漆，油漆等');

-- ----------------------------
-- Table structure for store_storemanage
-- ----------------------------
DROP TABLE IF EXISTS `store_storemanage`;
CREATE TABLE `store_storemanage`  (
  `storemanage_id` bigint(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '店铺id',
  `storemanage_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店名',
  `storemanage_content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主营',
  `storemanage_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `storemanage_notes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`storemanage_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of store_storemanage
-- ----------------------------
INSERT INTO `store_storemanage` VALUES (00000032, 'XXX水暖五金', '水暖配件，五金日用', '乌兰察布市集宁区XX街道XX号', '暂无备注');
INSERT INTO `store_storemanage` VALUES (00000033, 'XXX五金土产', '五金日用等', '呼和浩特市XX区XX街道XX号', '在呼市卖货的五金店');
INSERT INTO `store_storemanage` VALUES (00000034, 'XXX水暖卫浴', '管道、供暖设施、卫浴等', '巴彦淖尔市临河区XX街道XX号', '在巴彦淖尔市卖货的店');
INSERT INTO `store_storemanage` VALUES (00000036, 'XXX五金', '五金材料，建筑工具等', '呼和浩特', '在呼和浩特的店铺');
INSERT INTO `store_storemanage` VALUES (00000037, 'XX的任性吗？', 'XXX货物专卖', '集宁', '专卖XXX货物的店铺');

-- ----------------------------
-- Table structure for storehousemanagement
-- ----------------------------
DROP TABLE IF EXISTS `storehousemanagement`;
CREATE TABLE `storehousemanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `peifangmingcheng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peifangbianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peifangduixiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `peifangduixiangbianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of storehousemanagement
-- ----------------------------
INSERT INTO `storehousemanagement` VALUES (1, '1号配方', '1', '奶牛', '1-2');
INSERT INTO `storehousemanagement` VALUES (2, '6号配方', '2', '公牛', '1-3');
INSERT INTO `storehousemanagement` VALUES (3, '2号配方', '3', '公牛', '1-4');
INSERT INTO `storehousemanagement` VALUES (4, '3号配方', '4', '奶牛', '1-5');
INSERT INTO `storehousemanagement` VALUES (5, '4号配方', '5', '奶牛', '1-6');
INSERT INTO `storehousemanagement` VALUES (6, '5号配方', '6', '公牛', '1-7');

-- ----------------------------
-- Table structure for storemanagement
-- ----------------------------
DROP TABLE IF EXISTS `storemanagement`;
CREATE TABLE `storemanagement`  (
  `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `leixing` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuzhishu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zhuangtai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of storemanagement
-- ----------------------------
INSERT INTO `storemanagement` VALUES (1, '发情配种', '2', NULL);
INSERT INTO `storemanagement` VALUES (2, '初检', '1', NULL);
INSERT INTO `storemanagement` VALUES (3, '复检', '15', NULL);
INSERT INTO `storemanagement` VALUES (4, '围产', '12', NULL);
INSERT INTO `storemanagement` VALUES (5, '配次大于3妊娠提醒', '8', NULL);
INSERT INTO `storemanagement` VALUES (6, '处于情期牛', '9', NULL);
INSERT INTO `storemanagement` VALUES (7, 'PG/GNRH', '12', NULL);
INSERT INTO `storemanagement` VALUES (8, '临产提醒', '17', NULL);

-- ----------------------------
-- Table structure for transportationmanagement
-- ----------------------------
DROP TABLE IF EXISTS `transportationmanagement`;
CREATE TABLE `transportationmanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `jibingjilu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jibingcuhfang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chanhouhuli` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mangru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quchong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jianyi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of transportationmanagement
-- ----------------------------
INSERT INTO `transportationmanagement` VALUES (1, '乳房炎', '消灭病原微生物，控制炎症发展', '15', '25', '32', '12');
INSERT INTO `transportationmanagement` VALUES (2, '产后瘫痪', '使用乳房送风器向乳房打气，使乳房内压力增高，减少钙消耗', '15', '12', '10', '8');
INSERT INTO `transportationmanagement` VALUES (3, '胎衣不下', '子宫灌入抗菌药物', '20', '12', '16', '18');
INSERT INTO `transportationmanagement` VALUES (4, '奶牛不孕', '保证光照，通风良好，加强环境消毒', '16', '20', '10', '5');
INSERT INTO `transportationmanagement` VALUES (5, '酮病', '促肾上腺皮质激素治疗法', '16', '8', '16', '10');

-- ----------------------------
-- Table structure for unitmanagement
-- ----------------------------
DROP TABLE IF EXISTS `unitmanagement`;
CREATE TABLE `unitmanagement`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shouyaoshiyongjilu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jianyicishu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xiaoducishu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gongniubianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dongjingbianhao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinzhong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chushengriqi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of unitmanagement
-- ----------------------------
INSERT INTO `unitmanagement` VALUES (1, '1-120', '20', '15', '25', '1-110', '1-110', '短角牛', '2021-04-05');
INSERT INTO `unitmanagement` VALUES (2, '1-121', '25', '15', '12', '1-115', '1-115', '短角牛', '2021-03-15');
INSERT INTO `unitmanagement` VALUES (3, '1-122', '32', '20', '12', '1-111', '1-111', '短角牛', '2021-02-05');
INSERT INTO `unitmanagement` VALUES (4, '1-123', '30', '16', '20', '1-112', '1-112', '秦川牛', '2021-01-08');
INSERT INTO `unitmanagement` VALUES (5, '1-124', '11', '16', '8', '1-113', '1-113', '秦川牛', '2021-01-05');

-- ----------------------------
-- Table structure for untappedmilk
-- ----------------------------
DROP TABLE IF EXISTS `untappedmilk`;
CREATE TABLE `untappedmilk`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guanlihao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `niuzhishuliang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zongnailiang` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weiruguannai` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of untappedmilk
-- ----------------------------
INSERT INTO `untappedmilk` VALUES (1, '1-1201', '500', '5000', '1000');
INSERT INTO `untappedmilk` VALUES (2, '1-1202', '1000', '10000', '2000');
INSERT INTO `untappedmilk` VALUES (3, '3-1203', '1500', '15000', '3000');
INSERT INTO `untappedmilk` VALUES (4, '2-1204', '2000', '20000', '4000');
INSERT INTO `untappedmilk` VALUES (5, '1-1205', '2500', '25000', '5001');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_img` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `user_tel` bigint(20) NULL DEFAULT NULL COMMENT '用户电话（账号）',
  `user_password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `user_gender` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `user_email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `user_notes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `user_workstore` int(11) NULL DEFAULT NULL COMMENT '工作店铺（店铺id）',
  `user_appointment` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
  `user_state` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态（0：在岗、1：离职）',
  `user_identity` int(11) NULL DEFAULT NULL COMMENT '用户身份：0（管理员）1（销售员）',
  `user_createdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, NULL, 123456, '123456', '王七', '男', 'XXX@qq.com', '管理员', NULL, NULL, '0', 0, '2023-04-22 17:05:33');
INSERT INTO `user` VALUES (2, NULL, 12222222222, '111111', '李四', '男', 'XXX@qq.com', '他是李四，老板的亲戚，得给安排个店长', NULL, NULL, '0', 1, '2023-04-22 16:58:57');
INSERT INTO `user` VALUES (3, 'asdasd', 111111, '111111', '郭七', '男', 'asd@qq.com', '管理员', NULL, NULL, '0', 0, '2023-04-22 16:58:57');
INSERT INTO `user` VALUES (30, NULL, 15555555555, '111111', '张三', '男', 'XXX@qq.com', '他是张三，一个普通的员工', NULL, NULL, '0', 1, '2023-04-27 20:51:48');
INSERT INTO `user` VALUES (31, NULL, 13333333333, '111111', '王五', '男', 'XXX@qq.com', '他是王五，也是一个普通的员工', NULL, NULL, '0', 1, '2023-04-28 10:40:07');
INSERT INTO `user` VALUES (32, NULL, 14444444444, '111111', '马六', '女', 'XXX@qq.com', '她是马六，一个普通的女员工', NULL, NULL, '0', 1, '2023-04-28 10:40:49');
INSERT INTO `user` VALUES (33, 'asdasd', 16666666666, '111111', '张巴', '女', 'XXXX@qq.com', '她没什么特殊的', NULL, NULL, '1', 1, '2023-04-28 10:46:54');
INSERT INTO `user` VALUES (38, '', 999999, '999999', '某管理员', '男', '123@qq.com', '没什么好说的。', NULL, NULL, '0', 0, '2023-05-04 19:05:37');

SET FOREIGN_KEY_CHECKS = 1;
